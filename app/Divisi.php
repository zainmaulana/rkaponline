<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Divisi extends Model
{
    //
    use LogsActivity;

    protected $table = "tblm_divisi";

    protected $primaryKey = "id_divisi";

    protected $fillable = ['nama_divisi',
    'kode_divisi',
    'lantai_divisi',
    'image_divisi',
    'url_divisi',
    'no_urut_divisi'];

    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected static $logAttributes = ['nama_divisi',
    'kode_divisi',
    'lantai_divisi',
    'image_divisi',
    'url_divisi',
    'no_urut_divisi'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        >logFillable();
        // Chain fluent methods for configuration options
    }
}
