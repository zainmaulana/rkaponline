<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;

use App\TransaksiUpload;
use App\TransaksiUploadHistory;
use App\ScheduleClosing;

class ApprovalController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $listupload = DB::table('tblt_uploadrkapql as a')
        ->join('users as b','a.id_user_upload','=','b.id')
        ->leftJoin('users as c','a.id_user_approver','=','c.id')
        ->join('tblm_divisi as d','b.id_divisi','=','d.id_divisi')
        ->select('a.*',
        'b.name as nama_uploader',
        'd.nama_divisi',
        'c.name as nama_approver',
        DB::raw("(SELECT e.versi_file_dok
        FROM tblt_uploadrkapql_history e
        WHERE e.id_uploadrkapql = a.id_uploadrkapql
        ORDER BY e.versi_file_dok DESC
        LIMIT 1) as versi_dok"))
        ->orderBy('a.id_status_rkap','asc')
        ->get();

        //Tahun RKAP
        $tahun_rkap = Carbon::now('Asia/Jakarta')->year + 1;

        //Waktu Now
        $time_now =  Carbon::now('Asia/Jakarta');

        //Schedule Closing
        $schclosing = ScheduleClosing::where(
            [
                ['tahun_dok','=',$tahun_rkap],
                ['jenis_rkap','=','Kualitatif'],
                ['id_divisi','=',NULL]
            ])
        ->first();

        $host = request()->getSchemeAndHttpHost();

    	return view('monitoring_approval', compact('listupload', 'schclosing', 'time_now', 'host'));
    }

    public function approve_form($id, Request $data){

        $trapproveql = TransaksiUpload::find($id);
        $trapproveql->id_status_rkap = 2;
        $trapproveql->id_user_approver = Auth::user()->id;
        $trapproveql->last_updated_on = Carbon::now('Asia/Jakarta');
        $trapproveql->save();

        $idfiledok = TransaksiUploadHistory::where('id_uploadrkapql','=',$id)
        ->orderBy('versi_file_dok','desc')
        ->first();

        if($idfiledok <> NULL){
            TransaksiUploadHistory::create([
                'id_uploadrkapql' => $id,
                'file_dok' => $data->file_dok,
                'versi_file_dok' => $idfiledok->versi_file_dok + 1,
                'created_on' => Carbon::now()->addHours(7)
            ]);
        }

        else{
            TransaksiUploadHistory::create([
                'id_uploadrkapql' => $id,
                'file_dok' => $data->file_dok,
                'versi_file_dok' => 1,
                'created_on' => Carbon::now()->addHours(7)
            ]);
        }

        $trapprovesql2 = DB::table('tblt_uploadrkapql as a')
        ->join('users as b','a.id_user_upload','=','b.id')
        ->join('tblm_divisi as c','b.id_divisi','=','c.id_divisi')
        ->select('a.*',
        'b.name as nama_user',
        'b.email as email_user',
        'b.id_divisi',
        'c.nama_divisi')
        ->where('a.id_uploadrkapql','=',$id)
        ->first();

        //Tahun RKAP
        $tahun_rkap = $trapprovesql2->tahun_dok;

        //Send Email Content
        $host = request()->getSchemeAndHttpHost();
        $link = $host."/monitoring_upload";
        $to_name = $trapprovesql2->nama_user;
        $to_email = $trapprovesql2->email_user;
        $mail_template = 'mail_approved';
        $mail_subject = 'RKAP Divisi '.$trapprovesql2->nama_divisi.' tahun '.$tahun_rkap.' Sudah Disetujui';

        $dataEmail = array(
            "name" => $to_name,
            "body" => 'Berikut link Dokumen ('.$trapprovesql2->tipe_file_dok
            .') RKAP Divisi '.$trapprovesql2->nama_divisi
            .' tahun '.$tahun_rkap
            .' sudah disetujui oleh Tim RKAP',
            "link" => $link,
            "approved_date" => Carbon::now('Asia/Jakarta'),
            "nama_file" => $trapprovesql2->file_dok
        );

        app('App\Http\Controllers\UploadController')->send_mail_notif($dataEmail, $to_name, $to_email, $mail_template, $mail_subject);

        return redirect('/monitoring_approval');
    }

    public function open_approve_form($id, Request $data){

        $trapproveql = TransaksiUpload::find($id);
        $trapproveql->id_status_rkap = 3;
        $trapproveql->is_approved_atasan = FALSE;
        $trapproveql->id_user_approver = Auth::user()->id;
        $trapproveql->last_updated_on = Carbon::now('Asia/Jakarta');
        $trapproveql->save();

        $trapprovesql2 = DB::table('tblt_uploadrkapql as a')
        ->join('users as b','a.id_user_upload','=','b.id')
        ->join('tblm_divisi as c','b.id_divisi','=','c.id_divisi')
        ->select('a.*',
        'b.name as nama_user',
        'b.email as email_user',
        'b.id_divisi',
        'c.nama_divisi')
        ->where('a.id_uploadrkapql','=',$id)
        ->first();

        //Tahun RKAP
        $tahun_rkap = $trapprovesql2->tahun_dok;

        //Send Email Content
        $host = request()->getSchemeAndHttpHost();
        $link = $host."/monitoring_upload";
        $to_name = $trapprovesql2->nama_user;
        $to_email = $trapprovesql2->email_user;
        $mail_template = 'mail_openrkap';
        $mail_subject = 'RKAP Divisi '.$trapprovesql2->nama_divisi.' tahun '.$tahun_rkap.' dibuka untuk Revisi';

        $dataEmail = array(
            "name" => $to_name,
            "body" => 'Berikut link Dokumen ('.$trapprovesql2->tipe_file_dok
            .') RKAP Divisi '.$trapprovesql2->nama_divisi
            .' tahun '.$tahun_rkap
            .' dibuka kembali untuk direvisi terhadap poin - poin yang belum sesuai',
            "link" => $link,
            "open_date" => Carbon::now('Asia/Jakarta'),
            "closing_date" => $data->schclosing,
            "nama_file" => $trapprovesql2->file_dok
        );

        app('App\Http\Controllers\UploadController')->send_mail_notif($dataEmail, $to_name, $to_email, $mail_template, $mail_subject);

        return redirect('/monitoring_approval');
    }
}
