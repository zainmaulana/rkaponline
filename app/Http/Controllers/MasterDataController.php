<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Carbon\Carbon;
use File;

use App\User;
use App\Divisi;
use App\PermissionCustom;
use App\TemplateDokumen;
use App\ScheduleClosing;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class MasterDataController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

    }

    //User
    public function list_user(){

        $user = DB::table('users as a')
                  ->leftJoin('roles as b','a.id_role','=','b.id')
                  ->leftJoin('tblm_divisi as c','a.id_divisi','=','c.id_divisi')
                  ->select('a.*',
                  'b.id as id_role',
                  'b.name as nama_role',
                  'c.*')
                  ->orderBy('name','ASC')
                  ->get();

        $role = Role::all();

        $divisi = Divisi::all();

    	return view('masterdata.user', compact('user','role','divisi'));
    }

    public function insert_user(Request $data){

        //Validate Request
        $this->validate($data,[
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:8',
            'id_role' => 'required',
            'id_divisi' => 'required'
        ]);

        //Post Request
        $user = User::create([
            'name' => $data->name,
            'email' => $data->email,
            'username' => $data->username,
            'password' => Hash::make($data->password),
            'id_role' => $data->id_role,
            'id_divisi' => $data->id_divisi
        ]);

        $user->assignRole($data->id_role);

    	return redirect('/masterdata/user');
    }

    public function update_user($id, Request $data){

        //Validate Request
        $this->validate($data,[
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'id_role' => 'required',
            'id_divisi' => 'required'
        ]);

        //Update Request
        $user = User::find($id);
        $user->name = $data->name;
        $user->username = $data->username;
        $user->email = $data->email;
        $user->id_role = $data->id_role;
        $user->id_divisi = $data->id_divisi;

        if($data->password <> NULL){
            if($data->password_confirmation == $data->password){
                $user->password = Hash::make($data->password);
            }
            else{
                return redirect()->back()->with('error', 'Konfirmasi Password tidak sama');
            }
        }
        elseif($data->password == NULL || $data->password == ''){

        }

        $user->save();

        //Change Role
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($data->id_role);

        return redirect('/masterdata/user');
    }

    public function delete_user($id){

        $user = User::find($id);
        $user->removeRole($user->id_role);
        $user->delete();

        return redirect('/masterdata/user');
    }

    //Divisi
    public function list_divisi(){

        $divisi = Divisi::orderBy('no_urut_divisi','ASC')->get();

        return view('masterdata.divisi', compact('divisi'));
    }

    public function insert_divisi(Request $data){

        //Validate Request
        $this->validate($data,[
    		'nama_divisi' => 'required',
            'kode_divisi' => 'required',
            'lantai_divisi' => 'required',
            'no_urut_divisi' => 'required'
    	]);

        //File Attachment
        $file = $data->file('image_divisi');

        if($file <> NULL){
            $file_location = 'images/entitas';
            $file_name = $data->nama_divisi.'_'.$file->getClientOriginalName();
            $file->move($file_location, $file_name);

            //Post Request
            Divisi::create([
                'nama_divisi' => $data->nama_divisi,
                'kode_divisi' => $data->kode_divisi,
                'lantai_divisi' => $data->lantai_divisi,
                'image_divisi' => $file_name,
                'url_divisi' => $data->url_divisi,
                'no_urut_divisi' => $data->no_urut_divisi
            ]);
        }

        else{
            //Post Request
            Divisi::create([
                'nama_divisi' => $data->nama_divisi,
                'kode_divisi' => $data->kode_divisi,
                'lantai_divisi' => $data->lantai_divisi,
                'url_divisi' => $data->url_divisi,
                'no_urut_divisi' => $data->no_urut_divisi
            ]);
        }

    	return redirect('/masterdata/divisi');
    }

    public function update_divisi($id, Request $data){

        //Validate Request
        $this->validate($data,[
    		'nama_divisi' => 'required',
            'kode_divisi' => 'required',
            'lantai_divisi' => 'required',
            'no_urut_divisi' => 'required'
    	]);

        //Update Request
        $divisi = Divisi::find($id);
        $divisi->nama_divisi = $data->nama_divisi;
        $divisi->kode_divisi = $data->kode_divisi;
        $divisi->lantai_divisi = $data->lantai_divisi;
        $divisi->url_divisi = $data->url_divisi;
        $divisi->no_urut_divisi = $data->no_urut_divisi;

        //File Attachment
        $file = $data->file('image_divisi');

        if($file <> NULL){
            $file_location = 'images/entitas';
            $file_name = $data->nama_divisi.'_'.$file->getClientOriginalName();
            $file->move($file_location, $file_name);
            $divisi->image_divisi = $file_name;
        }

        $divisi->save();

        return redirect('/masterdata/divisi');
    }

    public function delete_divisi($id){

        $divisi = Divisi::find($id);
        $divisi->delete();

        return redirect('/masterdata/divisi');
    }

    //Permission
    public function list_permission(){

        $permission = Permission::all();

        $divisi = Divisi::all();

        $mappedpermission = DB::table('permissions as a')
        ->leftJoin('tblm_divisi as b','a.id_divisi','=','b.id_divisi')
        ->select('a.id',
        'a.name',
        'a.nama_menu',
        'a.nama_sub_menu',
        'b.id_divisi',
        'b.nama_divisi')
        ->get();

    	return view('masterdata.permission', compact('permission','divisi','mappedpermission'));
    }

    public function insert_permission(Request $data){

        //Validate Request
        $this->validate($data,[
    		'name' => 'required|unique:permissions',
    	]);

        //Post Request
        Permission::create([
    		'name' => $data->name,
        	'guard_name'=>'web'
    	]);

    	return redirect('/masterdata/permission');
    }

    public function update_permission($id, Request $data){

        //Validate Request
        $this->validate($data,[
    		'name' => 'required',
    	]);

        //Update Request
        $permission = PermissionCustom::find($id);
        $permission->name = $data->name;
        $permission->id_divisi = $data->id_divisi;
        $permission->nama_menu = $data->nama_menu;
        $permission->nama_sub_menu = $data->nama_sub_menu;
        $permission->save();

        return redirect('/masterdata/permission');
    }

    public function delete_permission($id){

        $permission = Permission::find($id);
        $permission->delete();

        return redirect('/masterdata/permission');
    }

    //Role
    public function list_role(){

        $role = Role::all();

        $permission = Permission::orderBy('name','ASC')->get();

        $rolepermission = DB::table('role_has_permissions as a')
                            ->join('roles as b','a.role_id','=','b.id')
                            ->join('permissions as c','a.permission_id','=','c.id')
                            ->select('a.role_id',
                            'a.permission_id',
                            'b.name as nama_role',
                            'c.name as nama_permission')
                            ->get();

    	return view('masterdata.role', compact('role','permission','rolepermission'));
    }

    public function insert_role(Request $data){

        //Validate Request
        $this->validate($data,[
    		'name' => 'required',
            'permission' => 'required'
    	]);

        //Post Request
        $role = Role::create([
    		'name' => $data->name
        ]);

        $role->syncPermissions($data->permission);

    	return redirect('/masterdata/role');
    }

    public function update_role($id, Request $data){

        //Validate Request
        $this->validate($data,[
            'name' => 'required',
            'permission' => 'required'
    	]);

        //Update Request
        $role = Role::find($id);
        $role->name = $data->name;
        $role->syncPermissions($data->permission);
        $role->save();

        return redirect('/masterdata/role');
    }

    public function delete_role($id){

        $role = Role::find($id);
        $role->delete();

        return redirect('/masterdata/role');
    }

    //Template Dokumen
    public function list_templatedok(){

        $divisi = Divisi::orderBy('nama_divisi','ASC')->get();

        $templatedivisi = DB::table('tblm_templatedokumen as a')
        ->join('tblm_divisi as b','a.id_divisi','=','b.id_divisi')
        ->select('a.id_template_dok',
        'a.nama_template_dok',
        'a.jenis_rkap',
        'a.tahun_dok',
        'a.tipe_file_dok',
        'a.file_dok',
        'a.created_on',
        'a.last_updated_on',
        'b.id_divisi',
        'b.nama_divisi')
        ->get();

    	return view('masterdata.templatedok', compact('divisi','templatedivisi'));
    }

    public function insert_templatedok(Request $data){

        //Validate Request
        $this->validate($data,[
            'id_divisi' => 'required',
    		'nama_template_dok' => 'required',
            'tahun_dok' => 'required|numeric|between:2021,2030',
            'file_dok' => 'file|mimes:docx,xlsx,pptx|max:204800'
    	]);

        //File Attachment
        $file = $data->file('file_dok');

        if($file <> NULL){
            //Get File Extension
            $tipe_file_dok = substr($file->getClientOriginalName(),-4);

            //Cek Jenis RKAP
            if($tipe_file_dok == 'xlsx'){
                if(substr($data->nama_template_dok,0,8) == 'Lampiran'){
                    $jenis_rkap = 'Kualitatif';
                }
                else{
                    $jenis_rkap = 'Kuantitatif';
                }
            }
            else{
                $jenis_rkap = 'Kualitatif';
            }

            $file_location = 'files/template_dok';
            $file_name = $data->nama_template_dok.'_'.$data->tahun_dok.'.'.$tipe_file_dok;
            $file->move($file_location, $file_name);

            //Post Request
            TemplateDokumen::create([
                'id_divisi' => $data->id_divisi,
                'nama_template_dok' => $data->nama_template_dok,
                'jenis_rkap' => $jenis_rkap,
                'tahun_dok' => $data->tahun_dok,
                'tipe_file_dok' => $tipe_file_dok,
                'file_dok' => $file_name,
                'created_on' => Carbon::now()->addHours(7)
            ]);
        }

        else{
            //Post Request
            TemplateDokumen::create([
                'id_divisi' => $data->id_divisi,
                'nama_template_dok' => $data->nama_template_dok,
                'tahun_dok' => $data->tahun_dok,
                'created_on' => Carbon::now()->addHours(7)
            ]);
        }

    	return redirect('/masterdata/templatedok');
    }

    public function update_templatedok($id, Request $data){

        //Validate Request
        $this->validate($data,[
            'id_divisi' => 'required',
    		'nama_template_dok' => 'required',
            'tahun_dok' => 'required|numeric|between:2021,2030',
            'file_dok' => 'file|mimes:docx,xlsx,pptx|max:204800'
    	]);

        //Update Request
        $tempdoc = TemplateDokumen::find($id);
        $tempdoc->id_divisi = $data->id_divisi;
        $tempdoc->nama_template_dok = $data->nama_template_dok;
        $tempdoc->tahun_dok = $data->tahun_dok;

        //File Attachment
        $file = $data->file('file_dok');

        if($file <> NULL){
            //Get File Extension
            $tipe_file_dok = substr($file->getClientOriginalName(),-4);

            //Cek Jenis RKAP
            if($tipe_file_dok == 'xlsx'){
                if(substr($data->nama_template_dok,0,8) == 'Lampiran'){
                    $jenis_rkap = 'Kualitatif';
                }
                else{
                    $jenis_rkap = 'Kuantitatif';
                }
            }
            else{
                $jenis_rkap = 'Kualitatif';
            }

            // Hapus File di Public Folder
            $tempdocs = TemplateDokumen::where('id_template_dok','=',$id)->first();
            File::delete('files/template_dok/'.$tempdocs->file_dok);

            $file_location = 'files/template_dok';
            $file_name = $data->nama_template_dok.'_'.$data->tahun_dok.'.'.$tipe_file_dok;
            $file->move($file_location, $file_name);

            $tempdoc->jenis_rkap = $jenis_rkap;
            $tempdoc->tipe_file_dok = $tipe_file_dok;
            $tempdoc->file_dok = $file_name;
        }

        $tempdoc->last_updated_on = Carbon::now('Asia/Jakarta');
        $tempdoc->save();

        return redirect('/masterdata/templatedok');
    }

    public function delete_templatedok($id){

        // Hapus File di Public Folder
        $tempdocs = TemplateDokumen::where('id_template_dok','=',$id)->first();
        File::delete('files/template_dok/'.$tempdocs->file_dok);

        $tempdocs = TemplateDokumen::find($id);
        $tempdocs->delete();

        return redirect('/masterdata/templatedok');
    }

    //Schedule Closing
    public function list_scheduleclosing(){

        $divisi = Divisi::orderBy('nama_divisi','ASC')->get();

        $scheduledivisi = DB::table('tblm_scheduleclosing as a')
        ->leftJoin('tblm_divisi as b','a.id_divisi','=','b.id_divisi')
        ->select('a.id_scheduleclosing',
        'a.jenis_rkap',
        'a.tahun_dok',
        'a.waktu_closing',
        'a.created_on',
        'a.last_updated_on',
        'a.id_divisi',
        'b.nama_divisi')
        ->get();

    	return view('masterdata.scheduleclosing', compact('divisi','scheduledivisi'));
    }

    public function insert_scheduleclosing(Request $data){

        //Validate Request
        $this->validate($data,[
    		'jenis_rkap' => 'required',
            'tahun_dok' => 'required|numeric|between:2021,2030',
            'waktu_closing' => 'required'
    	]);

        //Waktu Closing Global
        $globalclosing = ScheduleClosing::where(
            [
                ['tahun_dok','=',$data->tahun_dok],
                ['jenis_rkap','=',$data->jenis_rkap],
                ['id_divisi','=',NULL]
            ])
        ->first();

        //Waktu Closing Divisi
        $divisiclosing = ScheduleClosing::where(
            [
                ['tahun_dok','=',$data->tahun_dok],
                ['jenis_rkap','=',$data->jenis_rkap],
                ['id_divisi','=',$data->id_divisi]
            ])
        ->first();

        if($globalclosing == NULL || $divisiclosing == NULL){
            //Post Request
            ScheduleClosing::create([
                'id_divisi' => $data->id_divisi,
                'jenis_rkap' => $data->jenis_rkap,
                'tahun_dok' => $data->tahun_dok,
                'waktu_closing' => $data->waktu_closing,
                'created_on' => Carbon::now()->addHours(7)
            ]);
        }
        else{
            return redirect()->back()->with('error', 'Waktu Closing tersebut sudah ada');
        }

    	return redirect('/masterdata/scheduleclosing');
    }

    public function update_scheduleclosing($id, Request $data){

        //Validate Request
        $this->validate($data,[
    		'jenis_rkap' => 'required',
            'tahun_dok' => 'required|numeric|between:2021,2030',
    	]);

        //Waktu Closing Global
        $globalclosing = ScheduleClosing::where(
            [
                ['tahun_dok','=',$data->tahun_dok],
                ['jenis_rkap','=',$data->jenis_rkap],
                ['id_divisi','=',NULL]
            ])
        ->first();

        //Waktu Closing Divisi
        $divisiclosing = ScheduleClosing::where(
            [
                ['tahun_dok','=',$data->tahun_dok],
                ['jenis_rkap','=',$data->jenis_rkap],
                ['id_divisi','=',$data->id_divisi]
            ])
        ->first();

        //Update Request
        $tempdoc = ScheduleClosing::find($id);
        $tempdoc->id_divisi = $data->id_divisi;
        $tempdoc->jenis_rkap = $data->jenis_rkap;
        $tempdoc->tahun_dok = $data->tahun_dok;

        if($data->waktu_closing <> NULL){
            $tempdoc->waktu_closing = $data->waktu_closing;
        }

        $tempdoc->last_updated_on = Carbon::now('Asia/Jakarta');
        $tempdoc->save();

        return redirect('/masterdata/scheduleclosing');
    }

    public function delete_scheduleclosing($id){

        $tempdoc = ScheduleClosing::find($id);
        $tempdoc->delete();

        return redirect('/masterdata/scheduleclosing');
    }
}