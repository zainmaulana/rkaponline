<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;

use App\TransaksiUpload;
use App\TemplateDokumen;
use App\ScheduleClosing;
use App\User;

class UploadController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $listupload = DB::table('tblt_uploadrkapql as a')
        ->join('users as b','a.id_user_upload','=','b.id')
        ->leftJoin('users as c','a.id_user_approver','=','c.id')
        ->join('tblm_divisi as d','b.id_divisi','=','d.id_divisi')
        ->select('a.*',
        'b.name as nama_uploader',
        'd.nama_divisi',
        'c.name as nama_approver',
        DB::raw("(SELECT e.versi_file_dok
        FROM tblt_uploadrkapql_history e
        WHERE e.id_uploadrkapql = a.id_uploadrkapql
        ORDER BY e.versi_file_dok DESC
        LIMIT 1) as versi_dok"))
        ->where('b.id_divisi','=',Auth::user()->id_divisi)
        ->orderBy('a.id_status_rkap','desc')
        ->get();

        $dataupload = DB::table('users as a')
        ->join('tblm_divisi as b','a.id_divisi','=','b.id_divisi')
        ->select('a.name as nama_uploader',
        'b.id_divisi',
        'b.nama_divisi')
        ->where('a.id','=',Auth::user()->id)
        ->first();

        //Tahun RKAP
        $tahun_rkap = Carbon::now('Asia/Jakarta')->year + 1;

        //Waktu Now
        $time_now =  Carbon::now('Asia/Jakarta');

        //Schedule Closing
        $schclosing = ScheduleClosing::where(
            [
                ['tahun_dok','=',$tahun_rkap],
                ['jenis_rkap','=','Kualitatif'],
                ['id_divisi','=',NULL]
            ])
        ->first();

        $host = request()->getSchemeAndHttpHost();

    	return view('monitoring_upload', compact('listupload', 'dataupload', 'schclosing', 'time_now', 'host'));
    }

    public function form_upload(){

        $dataupload = DB::table('users as a')
        ->join('tblm_divisi as b','a.id_divisi','=','b.id_divisi')
        ->select('a.name as nama_uploader',
        'b.id_divisi',
        'b.nama_divisi')
        ->where('a.id','=',Auth::user()->id)
        ->first();

        //Tahun RKAP
        $tahun_rkap = Carbon::now('Asia/Jakarta')->year + 1;

        //Waktu Now
        $time_now =  Carbon::now('Asia/Jakarta');

        //Schedule Closing
        $schclosing = ScheduleClosing::where(
            [
                ['tahun_dok','=',$tahun_rkap],
                ['jenis_rkap','=','Kualitatif'],
                ['id_divisi','=',NULL]
            ])
        ->first();

        $host = request()->getSchemeAndHttpHost();

        return view('form_upload', compact('dataupload', 'tahun_rkap', 'schclosing', 'time_now', 'host'));
    }

    public function template_rkap(){

        $dataupload = DB::table('users as a')
        ->join('tblm_divisi as b','a.id_divisi','=','b.id_divisi')
        ->select('a.name as nama_uploader',
        'b.id_divisi',
        'b.nama_divisi')
        ->where('a.id','=',Auth::user()->id)
        ->first();

        //Tahun RKAP
        $tahun_rkap = Carbon::now('Asia/Jakarta')->year;

        $downloadform = TemplateDokumen::where(
            [
                // ['tahun_dok','=',$tahun_rkap],
                ['id_divisi','=',$dataupload->id_divisi]
            ])
        ->orderBy('jenis_rkap','asc')
        ->get();

        $host = request()->getSchemeAndHttpHost();

        return view('template_rkap', compact('dataupload', 'tahun_rkap', 'downloadform', 'host'));
    }

    public function upload_file(Request $data){

        //Validate Request
        $this->validate($data,[
            'file_dok' => 'file|mimes:docx,pptx,xlsx|max:204800',
            'catatan_upload' => 'required'
    	]);

        //Tahun RKAP
        $tahun_rkap = Carbon::now('Asia/Jakarta')->year + 1;

        //File Attachment & Get File Extension
        $file = $data->file('file_dok');
        $tipe_file_dok = substr($file->getClientOriginalName(),-4);

        //Get EVP
        $evp = User::where('id_divisi','=',$data->id_divisi)
        ->select('id',
        'name',
        'email',
        'id_divisi')
        ->first();

        //Cek Waktu Closing
        if($data->waktu_closing > Carbon::now('Asia/Jakarta')){
            //Cek File Upload
            if($file <> NULL){
                //Cek File Submit
                $lastfileupload = $this->get_last_file($tahun_rkap, $data->nama_divisi, $tipe_file_dok);

                if($lastfileupload <> NULL){
                    if($lastfileupload->id_status_rkap == 1 &&
                    $lastfileupload->is_approved_atasan == FALSE){
                        //Process Update Upload
                        $this->update_file_process($tahun_rkap, $data->nama_divisi, $tipe_file_dok, $data->catatan_upload,
                        $lastfileupload, $file);

                        $mail_subject = 'RKAP tahun '.$tahun_rkap.' sudah Diperbarui';
                    }
                    else{
                        return redirect()->back()->with('error', 'File sudah diupload/sedang diproses/sudah disetujui. Silahkan ke Halaman Monitoring');
                    }
                }
                else{
                    //Process Upload
                    $this->upload_file_process($tahun_rkap, $data->nama_divisi, $tipe_file_dok, $data->catatan_upload, $file);

                    $mail_subject = 'RKAP tahun '.$tahun_rkap.' sudah Diajukan';
                }

                //Send Email Content
                $host = request()->getSchemeAndHttpHost();
                $link = $host."/monitoring_submit";
                $to_name = $evp->name;
                $to_email = $evp->email;
                $mail_template = 'mail_to_evp';

                $dataEmail = array(
                    "name" => $evp->name,
                    "body" => 'Berikut link Dokumen ('.$tipe_file_dok
                    .') RKAP Divisi '.$data->nama_divisi
                    .' tahun '.$tahun_rkap
                    .' yang sudah diupload. Mohon untuk mengecek dokumen berikut sebelum dilakukan Approval dan diteruskan ke Divisi Corporate Planning',
                    "link" => $link
                );

                // $this->send_mail_notif($dataEmail, $to_name, $to_email, $mail_template, $mail_subject);
            }
            else{
                return redirect()->back()->with('error', 'File belum dilampirkan');
            }
        }
        else{
            return redirect()->back()->with('error', 'Sudah melewati Waktu Closing');
        }

    	return redirect('/monitoring_upload');
    }

    public function get_last_file(int $tahun_rkap, String $nama_divisi, String $tipe_file_dok){

        //Get Last File
        $lastfileupload = TransaksiUpload::join('users as b','tblt_uploadrkapql.id_user_upload','=','b.id')
        ->join('tblm_divisi as c','b.id_divisi','=','c.id_divisi')
        ->where(
            [
                ['tahun_dok','=',$tahun_rkap],
                ['c.nama_divisi','=',$nama_divisi],
                ['tipe_file_dok','=',$tipe_file_dok]
            ])
        ->first();

        return $lastfileupload;
    }

    public function upload_file_process(int $tahun_rkap, String $nama_divisi, String $tipe_file_dok,
    String $catatan_upload, Object $file){

        //Get File
        $file_location = 'files/upload_dok';

        if($tipe_file_dok == 'xlsx'){
            $file_name = 'Lampiran_RKAP_'.$tahun_rkap.'_'.$nama_divisi.'.'.$tipe_file_dok;
        }
        else{
            $file_name = 'RKAP_'.$tahun_rkap.'_'.$nama_divisi.'.'.$tipe_file_dok;
        }

            //Post Request
            TransaksiUpload::create([
                'id_user_upload' => Auth::user()->id,
                'id_status_rkap' => 1,//Submitted
                'tahun_dok' => $tahun_rkap,
                'tipe_file_dok' => $tipe_file_dok,
                'file_dok' => $file_name,
                'catatan_upload' => $catatan_upload,
                'is_approved_atasan' => FALSE,
                'created_on' => Carbon::now()->addHours(7)
            ]);

        //Store File
        $file->move($file_location, $file_name);
    }

    public function update_file_process(int $tahun_rkap, String $nama_divisi, String $tipe_file_dok,
    String $catatan_upload, Object $lastfileupload, Object $file){

        //Get File
        $file_location = 'files/upload_dok';

        if($tipe_file_dok == 'xlsx'){
            $file_name = 'Lampiran_RKAP_'.$tahun_rkap.'_'.$nama_divisi.'.'.$tipe_file_dok;
        }
        else{
            $file_name = 'RKAP_'.$tahun_rkap.'_'.$nama_divisi.'.'.$tipe_file_dok;
        }

            //Update Request
            $truploadql = TransaksiUpload::find($lastfileupload->id_uploadrkapql);
            $truploadql->file_dok = $file_name;
            $truploadql->catatan_upload = $catatan_upload;
            $truploadql->last_updated_by = Auth::user()->name;
            $truploadql->last_updated_on = Carbon::now('Asia/Jakarta');
            $truploadql->save();

        //Store File
        $file->move($file_location, $file_name);
    }

    public function upload_revision($id, Request $data){

        //Validate Request
        $this->validate($data,[
            'file_dok' => 'file|mimes:docx,pptx,xlsx|max:204800',
            'catatan_upload' => 'required'
    	]);

        //Tahun RKAP
        $tahun_rkap = Carbon::now('Asia/Jakarta')->year + 1;

        //Versi Terakhir
        $last_versi = DB::table('tblt_uploadrkapql as a')
        ->join('tblt_uploadrkapql_history as b','a.id_uploadrkapql','=','b.id_uploadrkapql')
        ->select('b.versi_file_dok',
        'a.tipe_file_dok')
        ->where('a.id_uploadrkapql','=',$data->id_uploadrkapql)
        ->orderBy('b.versi_file_dok','desc')
        ->first();

        //File Attachment & Get File Extension
        $file = $data->file('file_dok');
        $tipe_file_dok = substr($file->getClientOriginalName(),-4);

        //Cek Last Versi RKAP
        if($last_versi <> NULL){
            //Cek Waktu Closing
            if($data->waktu_closing > Carbon::now('Asia/Jakarta')){
                //Cek File Upload
                if($file <> NULL && $last_versi->tipe_file_dok == $tipe_file_dok){

                    //Get Last File
                    $lastfileupload = $this->get_last_file($tahun_rkap, $data->nama_divisi, $tipe_file_dok);

                    //Increment Versi
                    $new_versi = $last_versi->versi_file_dok + 1;

                    //Get File
                    $file_location = 'files/upload_dok';

                    if($tipe_file_dok == 'xlsx'){
                        $file_name = 'Lampiran_RKAP_'.$tahun_rkap.'_'.$data->nama_divisi.'_v'.$new_versi.'.'.$tipe_file_dok;
                    }
                    else{
                        $file_name = 'RKAP_'.$tahun_rkap.'_'.$data->nama_divisi.'_v'.$new_versi.'.'.$tipe_file_dok;
                    }

                    //Cek Upload File Terakhir
                    if($lastfileupload <> NULL){
                        //Update Request
                        $truploadql = TransaksiUpload::find($lastfileupload->id_uploadrkapql);
                        $truploadql->id_status_rkap = 4;
                        $truploadql->file_dok = $file_name;
                        $truploadql->catatan_upload = $data->catatan_upload;
                        $truploadql->last_updated_by = Auth::user()->name;
                        $truploadql->last_updated_on = Carbon::now('Asia/Jakarta');
                        $truploadql->save();

                        //Store File
                        $file->move($file_location, $file_name);

                        //Get EVP
                        $evp = User::where('id_divisi','=',$data->id_divisi)
                        ->select('id',
                        'name',
                        'email',
                        'id_divisi')
                        ->first();

                        //Send Email Content
                        $host = request()->getSchemeAndHttpHost();
                        $link = $host."/monitoring_submit";
                        $to_name = $evp->name;
                        $to_email = $evp->email;
                        $mail_template = 'mail_submitted';
                        $mail_subject = 'RKAP tahun '.$tahun_rkap.' sudah Direvisi dan Diajukan Kembali';

                        $dataEmail = array(
                            "name" => $to_name,
                            "body" => 'Berikut link Dokumen ('.$tipe_file_dok
                            .') RKAP Divisi '.$data->nama_divisi
                            .' tahun '.$tahun_rkap
                            .' yang sudah diupload. Mohon untuk mengecek dokumen berikut sebelum dilakukan Approval dan diteruskan ke Divisi Corporate Planning',
                            "link" => $link,
                            "uploader" => $lastfileupload->name,
                            "nama_file" => $lastfileupload->file_dok
                        );

                        // $this->send_mail_notif($dataEmail, $to_name, $to_email, $mail_template, $mail_subject);
                    }
                }
                else{
                    return redirect()->back()->with('error', 'File belum dilampirkan/ekstensi tidak sesuai');
                }
            }
            else{
                return redirect()->back()->with('error', 'Sudah melewati Waktu Closing');
            }
        }
        else{
            return redirect()->back();
        }

    	return redirect('/monitoring_upload');
    }

    public function monitoring_submit(){

        $listupload = DB::table('tblt_uploadrkapql as a')
        ->join('users as b','a.id_user_upload','=','b.id')
        ->leftJoin('users as c','a.id_user_approver','=','c.id')
        ->join('tblm_divisi as d','b.id_divisi','=','d.id_divisi')
        ->select('a.*',
        'b.name as nama_uploader',
        'd.nama_divisi',
        'c.name as nama_approver',
        DB::raw("(SELECT e.versi_file_dok
        FROM tblt_uploadrkapql_history e
        WHERE e.id_uploadrkapql = a.id_uploadrkapql
        ORDER BY e.versi_file_dok DESC
        LIMIT 1) as versi_dok"))
        ->where('b.id_divisi','=',Auth::user()->id_divisi)
        ->orderBy('a.id_status_rkap','desc')
        ->get();

        $dataupload = DB::table('users as a')
        ->join('tblm_divisi as b','a.id_divisi','=','b.id_divisi')
        ->select('a.name as nama_uploader',
        'b.id_divisi',
        'b.nama_divisi')
        ->where('a.id','=',Auth::user()->id)
        ->first();

        $host = request()->getSchemeAndHttpHost();

    	return view('monitoring_submit', compact('listupload', 'dataupload', 'host'));
    }

    public function approve_submit($id){

        $trapprovesql = TransaksiUpload::find($id);
        $trapprovesql->is_approved_atasan = TRUE;
        $trapprovesql->last_updated_on = Carbon::now('Asia/Jakarta');
        $trapprovesql->save();

        $trapprovesql2 = DB::table('tblt_uploadrkapql as a')
        ->join('users as b','a.id_user_upload','=','b.id')
        ->join('tblm_divisi as c','b.id_divisi','=','c.id_divisi')
        ->select('a.*',
        'b.name',
        'b.email',
        'b.id_divisi',
        'c.nama_divisi')
        ->where('a.id_uploadrkapql','=',$id)
        ->first();

        //Tahun RKAP
        $tahun_rkap = $trapprovesql2->tahun_dok;

        //Get Admin RKAP
        $adminrkap = User::join('roles as b','users.id_role','=','b.id')
        ->where('b.name','=','Admin')
        ->select('users.id as id_user',
        'users.name as nama_admin',
        'email as email_admin')
        ->get();

        foreach($adminrkap as $p){

            //Send Email Content to Admin
            $host = request()->getSchemeAndHttpHost();
            $link = $host."/monitoring_approval";
            $to_name = $p->nama_admin;
            $to_email = $p->email_admin;
            $mail_template = 'mail_submitted';
            $mail_subject = 'RKAP Divisi '.$trapprovesql2->nama_divisi.' tahun '.$tahun_rkap.' sudah Diajukan dan Disetujui EVP';

            $dataEmail = array(
                "name" => $to_name,
                "body" => 'Berikut link Dokumen ('.$trapprovesql2->tipe_file_dok
                .') RKAP Divisi '.$trapprovesql2->nama_divisi
                .' tahun '.$tahun_rkap
                .' yang sudah diupload dan disetujui EVP terkait. Mohon untuk mengecek dokumen berikut sebelum dilakukan Approval',
                "link" => $link,
                "uploader" => $trapprovesql2->name,
                "nama_file" => $trapprovesql2->file_dok
            );

            $this->send_mail_notif($dataEmail, $to_name, $to_email, $mail_template, $mail_subject);
        }

        //Send Email Content to User
        $host2 = request()->getSchemeAndHttpHost();
        $link2 = $host2."/monitoring_upload";
        $to_name2 = $trapprovesql2->name;
        $to_email2 = $trapprovesql2->email;
        $mail_template2 = 'mail_to_evp';
        $mail_subject2 = 'RKAP Divisi '.$trapprovesql2->nama_divisi.' tahun '.$tahun_rkap.' sudah Diajukan dan Disetujui EVP';

        $dataEmail2 = array(
            "name" => $to_name2,
            "body" => 'Berikut link Dokumen ('.$trapprovesql2->tipe_file_dok
            .') RKAP Divisi '.$trapprovesql2->nama_divisi
            .' tahun '.$tahun_rkap
            .' yang sudah diupload dan disetujui EVP terkait. Mohon untuk mengecek dokumen berikut sebelum dilakukan Approval',
            "link" => $link2
        );

        $this->send_mail_notif($dataEmail2, $to_name2, $to_email2, $mail_template2, $mail_subject2);

        return redirect('/monitoring_submit');
    }

    public function send_mail_notif(Array $dataEmail, String $to_name, String $to_email,
    String $mail_template, String $mail_subject){

        $email = Mail::send('emails.'.$mail_template, $dataEmail, function($message) use ($to_name, $to_email, $mail_subject) {
            $message->to($to_email, $to_name)->subject('SIRENS (Sistem Integrasi Rencana Strategis) - '.$mail_subject);
            $message->from('admin.servicedesk@hutamakarya.com','SIRENS');
        });
    }
}
