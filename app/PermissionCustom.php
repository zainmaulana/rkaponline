<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;


class PermissionCustom extends Model
{
    //
    protected $table = "permissions";

    protected $primaryKey = "id";

    protected $fillable = ['name',
    'guard_name',
    'id_divisi',
    'nama_menu',
    'nama_sub_menu'];

    protected static $logAttributes = ['name',
    'guard_name',
    'id_divisi',
    'nama_menu',
    'nama_sub_menu'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        >logFillable();
        // Chain fluent methods for configuration options
    }
}