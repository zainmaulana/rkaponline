<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class ScheduleClosing extends Model
{
    //
    use LogsActivity;

    protected $table = "tblm_scheduleclosing";

    protected $primaryKey = "id_scheduleclosing";

    protected $fillable = ['id_divisi',
    'jenis_rkap',
    'tahun_dok',
    'waktu_closing'];

    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'last_updated_on';

    protected static $logAttributes = ['id_divisi',
    'jenis_rkap',
    'tahun_dok',
    'waktu_closing'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        >logFillable();
        // Chain fluent methods for configuration options
    }
}
