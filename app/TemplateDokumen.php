<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class TemplateDokumen extends Model
{
    //
    use LogsActivity;

    protected $table = "tblm_templatedokumen";

    protected $primaryKey = "id_template_dok";

    protected $fillable = ['id_divisi',
    'nama_template_dok',
    'jenis_rkap',
    'tahun_dok',
    'tipe_file_dok',
    'file_dok'];

    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'last_updated_on';

    protected static $logAttributes = ['id_divisi',
    'nama_template_dok',
    'jenis_rkap',
    'tahun_dok',
    'tipe_file_dok',
    'file_dok'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        >logFillable();
        // Chain fluent methods for configuration options
    }
}
