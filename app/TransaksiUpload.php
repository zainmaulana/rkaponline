<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class TransaksiUpload extends Model
{
    //
    use LogsActivity;

    protected $table = "tblt_uploadrkapql";

    protected $primaryKey = "id_uploadrkapql";

    protected $fillable = ['id_user_upload',
    'id_status_rkap',
    'id_user_approver',
    'tahun_dok',
    'tipe_file_dok',
    'file_dok',
    'catatan_upload',
    'is_approved_atasan',
    'last_updated_by'];

    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'last_updated_on';

    protected static $logAttributes = ['id_user_upload',
    'id_status_rkap',
    'id_user_approver',
    'tahun_dok',
    'tipe_file_dok',
    'file_dok',
    'catatan_upload',
    'is_approved_atasan',
    'last_updated_by'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        >logFillable();
        // Chain fluent methods for configuration options
    }
}
