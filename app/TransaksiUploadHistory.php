<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class TransaksiUploadHistory extends Model
{
    //
    use LogsActivity;

    protected $table = "tblt_uploadrkapql_history";

    protected $primaryKey = "id_uploadrkapql_history";

    protected $fillable = ['id_uploadrkapql',
    'file_dok',
    'versi_file_dok'];

    const CREATED_AT = 'created_on';
    const UPDATED_AT = NULL;

    protected static $logAttributes = ['id_uploadrkapql',
    'file_dok',
    'versi_file_dok'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        >logFillable();
        // Chain fluent methods for configuration options
    }
}
