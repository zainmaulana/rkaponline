<div class="navbar navbar-light">
    <!--<div class="text-center d-lg-none w-100">-->
    <!--    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">-->
    <!--        <i class="icon-unfold mr-2"></i>-->
    <!--        Footer-->
    <!--    </button>-->
    <!--</div>-->

    <div class="navbar" id="navbar-footer">
        <span class="navbar-text">
            &copy; 2021 - Bagian Teknologi Informasi, <a href="https://www.hutamakarya.com" target="_blank">PT Hutama Karya (Persero)</a>
        </span>
    </div>
</div>