<?php
    use Illuminate\Support\Facades\DB;

    $iduser = Auth::user()->id;

        //Nama User
        $user = DB::table('users as a')
        ->leftJoin('roles as b','a.id_role','=','b.id')
        ->leftJoin('tblm_divisi as c','a.id_divisi','=','c.id_divisi')
    	->select('a.*',
        'b.id as id_role',
    	'b.name as nama_role',
        'c.*')
		->where('a.id','=',$iduser)
        ->orderBy('name','ASC')
        ->first();
?>

<nav id="sidebar">
    <div class="sidebar-header">
        <center>
            <a class="navbar-brand" href="{{ url('/') }}">
                <h3>{{ config('app.name', 'SIRENS') }}</h3>
            </a>
        </center>
    </div>

    <ul class="list-unstyled components">
        <li>
            <a href="/"><i class="fa fa-home"></i>&emsp; Home</a>
        </li>

        @if($user->nama_role == "User")
        <li>
            <a href="/form_upload"><i class="fa fa-file-text-o"></i>&emsp; Form Upload</a>
        </li>
        <li>
            <a href="/template_rkap"><i class="fa fa-sticky-note-o"></i>&emsp; Template RKAP</a>
        </li>
        @endif

        <li>
            <a href="#monitoring" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fa fa-desktop" aria-hidden="true"></i>&emsp; Monitoring
            </a>
            <ul class="collapse list-unstyled" id="monitoring">
                @if($user->nama_role == "User")
                <li>
                    <a href="/monitoring_upload">User View</a>
                </li>
                @elseif($user->nama_role == "EVP")
                <li>
                    <a href="/monitoring_submit">EVP View</a>
                </li>
                @elseif($user->nama_role == "Admin")
                <li>
                    <a href="/monitoring_approval">Admin View</a>
                </li>
                @endif
            </ul>
        </li>

        @if($user->nama_role == "Admin")
        <li>
            <a href="#masterdata" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fa fa-database" aria-hidden="true"></i>&emsp; Master Data
            </a>
            <ul class="collapse list-unstyled" id="masterdata">
                <li>
                    <a href="/masterdata/user">User</a>
                </li>
                <li>
                    <a href="/masterdata/divisi">Divisi</a>
                </li>
                <li>
                    <a href="/masterdata/permission">Permission</a>
                </li>
                <li>
                    <a href="/masterdata/role">Role</a>
                </li>
                <li>
                    <a href="/masterdata/templatedok">Template RKAP</a>
                </li>
                <li>
                    <a href="/masterdata/scheduleclosing">Schedule Closing</a>
                </li>
            </ul>
        </li>
        @endif
    </ul>

    <ul class="list-unstyled CTAs">
        <li>
            <center>
                &copy; 2021 - Divisi Corporate Planning & Divisi Sistem, IT & Riset Teknologi
            </center>
        </li>
    </ul>
</nav>