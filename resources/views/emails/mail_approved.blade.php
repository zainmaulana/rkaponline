<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>

<style>
    table {
      border-collapse: collapse;
    }

    th, td {
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even){background-color: #f2f2f2}

    th {
      background-color: #424242;
      color: white;
    }
</style>

<body>
    <table>
		<tr>
			<td><h2 align="left">Halo <strong>{{ $name }} !</strong></h2></td>
		</tr>
	</table>

	<p>
		<h3 align="center">{{ $body }}</h3>
		<center>
			<a href="{{$link}}">{{$link}}</a>
		</center>
		<br>
		<center>
		    <table>
                <thead>
                    <th style="width: 40%">Tanggal Disetujui</th>
                    <th style="width: 60%">Nama File</th>
                </thead>
                <tr>
                    <td>{{ $approved_date }}</td>
                    <td>{{ $nama_file }}</td>
                </tr>
            </table>
		</center>
	</p>

	<p>Selalu cek update terbaru dari RKAP yang sudah diajukan.</p>

	<p>
        Best Regards,<br/>
        Divisi Corporate Planning, <br/>
        PT Hutama Karya (Persero)
    </p>
	</body>
</html>
