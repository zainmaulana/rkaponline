@extends('layouts.app_custom')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-primary mb-3">
                    <h5 class="card-title">Form Upload RKAP {{ $dataupload->nama_divisi }} tahun {{ $tahun_rkap }}</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if($schclosing)
                                <h5>Schedule Closing RKAP : {{ $schclosing->waktu_closing }}</h5>
                            @endif
                        </div>
                        <!-- <div class="col-md-6 d-flex justify-content-center">
                            <h5>Waktu Sekarang : {{ $time_now }}</h5>
                        </div> -->
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <form action="/upload_file" method="post" enctype="multipart/form-data">

                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="file_attach">Upload File RKAP</label>
                                        <div class="custom-file" id="file_attach">
                                            <input type="file" class="custom-file-input" id="customFile" name="file_dok" required>
                                            <label class="custom-file-label" for="customFile">Pilih File</label>
                                        </div>
                                        <input type="hidden" name="nama_divisi" value="{{ $dataupload->nama_divisi }}">
                                        <input type="hidden" name="id_divisi" value="{{ $dataupload->id_divisi }}">
                                </div>
                                <div class="form-group">
                                    <label for="catatan_upload">Catatan Upload File RKAP</label>
                                    <textarea class="form-control" id="catatan_upload" name="catatan_upload" rows="3" required></textarea>
                                </div>

                                @if($schclosing)
                                    <input type="hidden" name="waktu_closing" value="{{ $schclosing->waktu_closing }}">
                                @endif

                                <div class="d-flex justify-content-center">
                                    @if($schclosing)
                                        @if($schclosing->waktu_closing > $time_now)
                                            <button type="submit" class="btn btn-primary" style="width: 36rem;">Submit</button>
                                        @else
                                            <button type="submit" class="btn btn-primary" style="width: 36rem;" disabled>Closed</button>
                                        @endif
                                    @else
                                        Waktu Closing Belum Ada
                                    @endif
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

		@if (count($errors) > 0)
            <div class="alert alert-danger">
	            <ul>
		            @foreach ($errors->all() as $error)
		            	<li>{{ $error }}</li>
		            @endforeach
	            </ul>
            </div>
        @endif

    <script type="text/javascript">
        $('#customFile').on('change',function(){
			//get the file name
			var fileName = $(this).val().replace('C:\\fakepath\\', " ");
			//replace the "Choose a file" label
			$(this).next('.custom-file-label').html(fileName);
		})

        var timestamp = '<?=time();?>';
        function updateTime(){
        $('#time').html(Date(timestamp));
        timestamp++;
        }
        $(function(){
        setInterval(updateTime, 1000);
        });
	</script>
@endsection