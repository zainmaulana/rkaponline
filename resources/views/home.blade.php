@extends('layouts.app_custom')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Logged Status</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Selamat Datang di Aplikasi Sistem Integrasi Rencana Strategis (SIRENS)
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
