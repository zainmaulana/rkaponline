<!-- Bootstrap css -->
<link href="../templatelogin/plugins/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" />

<!-- Style css -->
<link href="../templatelogin/css/style.css" rel="stylesheet" />
<link href="../templatelogin/css/skin-modes.css" rel="stylesheet" />

<!-- Font-awesome  css -->
<link href="../css/icons.css" rel="stylesheet"/>

<!--Horizontal Menu css-->
<link href="../templatelogin/plugins/horizontal-menu/horizontal-menu.css" rel="stylesheet" />

<!--Select2 css -->
<link href="../templatelogin/plugins/select2/select2.min.css" rel="stylesheet" />

<!-- Cookie css -->
<!-- <link href="../plugins/cookie/cookie.css" rel="stylesheet"> -->

<!-- Owl Theme css-->
<link href="../templatelogin/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" />

<!-- Custom scroll bar css-->
<link href="../templatelogin/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

<!-- Pscroll bar css-->
<link href="../templatelogin/plugins/pscrollbar/pscrollbar.css" rel="stylesheet" />

<!-- Switcher css -->
<link  href="../templatelogin/switcher/css/switcher.css" rel="stylesheet" id="switcher-css" type="text/css" media="all"/>

<!-- Color Skin css -->
<link id="theme" rel="stylesheet" type="text/css" media="all" href="../templatelogin/color-skins/color6.css" />