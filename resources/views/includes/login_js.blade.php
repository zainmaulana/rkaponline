<!-- JQuery js-->
<script src="../templatelogin/js/jquery-3.2.1.min.js"></script>

<!-- Bootstrap js -->
<script src="../templatelogin/plugins/bootstrap-4.3.1/js/popper.min.js"></script>
<script src="../templatelogin/plugins/bootstrap-4.3.1/js/bootstrap.min.js"></script>

<!--JQuery Sparkline js-->
<script src="../templatelogin/js/jquery.sparkline.min.js"></script>

<!-- Circle Progress js-->
<script src="../templatelogin/js/circle-progress.min.js"></script>

<!-- Star Rating js-->
<script src="../templatelogin/plugins/rating/jquery.rating-stars.js"></script>

<!--Counters js-->
<script src="../templatelogin/plugins/counters/counterup.min.js"></script>
<script src="../templatelogin/plugins/counters/waypoints.min.js"></script>
<script src="../templatelogin/plugins/counters/numeric-counter.js"></script>

<!--Owl Carousel js -->
<script src="../templatelogin/plugins/owl-carousel/owl.carousel.js"></script>

<!--Horizontal Menu js-->
<script src="../templatelogin/plugins/horizontal-menu/horizontal-menu.js"></script>

<!--JQuery TouchSwipe js-->
<script src="../templatelogin/js/jquery.touchSwipe.min.js"></script>

<!--Select2 js -->
<script src="../templatelogin/plugins/select2/select2.full.min.js"></script>
<script src="../templatelogin/js/select2.js"></script>

<!-- sticky js-->
<script src="../templatelogin/js/sticky.js"></script>

<!-- Pscrollbar js -->
<script src="../templatelogin/plugins/pscrollbar/pscrollbar.js"></script>
<script src="../templatelogin/plugins/pscrollbar/pscroll.js"></script>

<!-- Cookie js -->
<!-- <script src="../plugins/cookie/jquery.ihavecookies.js"></script>
<script src="../plugins/cookie/cookie.js"></script> -->

<!-- Custom scroll bar js-->
<script src="../templatelogin/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Swipe js-->
<script src="../templatelogin/js/swipe.js"></script>

<!-- Scripts js-->
<script src="../templatelogin/js/owl-carousel.js"></script>

<!-- Custom js-->
<script src="../templatelogin/js/custom.js"></script>