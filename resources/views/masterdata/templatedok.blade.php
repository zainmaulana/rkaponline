@extends('layouts.app_custom')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-primary mb-3">
                    <h5 class="card-title">Master Data Template Dokumen</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addData">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah Data</button>
                        </div>
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <table id="table_id" class="table table-striped table-bordered nowrap" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Opsi</th>
                                        <th>Divisi</th>
                                        <th>Nama Template</th>
                                        <th>Jenis RKAP</th>
                                        <th>Tahun RKAP</th>
                                        <th>Nama File</th>
                                        <th>Tanggal Dibuat</th>
                                        <th>Tanggal Diperbarui</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $n = 1?>
                                    @foreach($templatedivisi as $p)
                                    <tr>
                                        <td>{{ $n++ }}</td>
                                        <td>
                                            <center>
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#editData_Update{{ $p->id_template_dok }}">
                                                <i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#editData_Delete{{ $p->id_template_dok }}">
                                                <i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </center>
                                        </td>
                                        <td>{{ $p->nama_divisi }}</td>
                                        <td>{{ $p->nama_template_dok }}</td>
                                        <td>{{ $p->jenis_rkap }}</td>
                                        <td>{{ $p->tahun_dok }}</td>
                                        <td>{{ $p->file_dok }}</td>
                                        <td>{{ $p->created_on }}</td>
                                        <td>{{ $p->last_updated_on }}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>

                        <div id="addData" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/templatedok/insert" method="post" enctype="multipart/form-data">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="id_divisi">Divisi</label>
                                            <select class="form-control" name="id_divisi" required="required">
                                                @foreach($divisi as $p)
                                                    <option value="{{ $p->id_divisi }}">{{ $p->nama_divisi }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama_template_dok">Nama Template</label>
                                            <input type="text" class="form-control" name="nama_template_dok" placeholder="Tidak Boleh ada Spasi" value="{{ old('nama_template_dok') }}">
                                        </div>
                                        <!-- <div class="form-group">
                                            <label for="jenis_rkap">Jenis RKAP</label>
                                            <select class="form-control" name="jenis_rkap" required="required">
                                                <option value="Kuantitatif">Kuantitatif</option>
                                                <option value="Kualitatif">Kualitatif</option>
                                            </select>
                                        </div> -->
                                        <div class="form-group">
                                            <label for="tahun_dok">Tahun RKAP</label>
                                            <input type="number" class="form-control" name="tahun_dok" value="{{ old('tahun_dok') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="file_attach">File Template RKAP</label>
                                                <div class="custom-file" id="file_attach">
                                                    <input type="file" class="custom-file-input" id="customFile" name="file_dok">
                                                    <label class="custom-file-label" for="customFile">Pilih File</label>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-success">Tambah</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        @foreach($templatedivisi as $u)
                        <div id="editData_Update{{ $u->id_template_dok }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/templatedok/update{{ $u->id_template_dok }}" method="post" enctype="multipart/form-data">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Edit Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="id_divisi">Divisi</label>
                                            <select class="form-control" name="id_divisi" required="required">
                                                <option value="{{ $u->id_divisi }}">{{ $u->nama_divisi }}</option>
                                                @foreach($divisi as $p)
                                                    <option value="{{ $p->id_divisi }}">{{ $p->nama_divisi }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama_template_dok">Nama Template</label>
                                            <input type="text" class="form-control" name="nama_template_dok" placeholder="Tidak Boleh ada Spasi" value="{{ $u->nama_template_dok }}">
                                        </div>
                                        <!-- <div class="form-group">
                                            <label for="jenis_rkap">Jenis RKAP</label>
                                            <select class="form-control" name="jenis_rkap" required="required">
                                                <option value="{{ $u->jenis_rkap }}">{{ $u->jenis_rkap }}</option>
                                                <option value="Kuantitatif">Kuantitatif</option>
                                                <option value="Kualitatif">Kualitatif</option>
                                            </select>
                                        </div> -->
                                        <div class="form-group">
                                            <label for="tahun_dok">Tahun RKAP</label>
                                            <input type="number" class="form-control" name="tahun_dok" value="{{ $u->tahun_dok }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="file_attach">File Template RKAP</label>
                                                <div class="custom-file" id="file_attach">
                                                    <input type="file" class="custom-file-input" id="customFile" name="file_dok">
                                                    <label class="custom-file-label" for="customFile">Pilih File</label>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-success">Update</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        @foreach($templatedivisi as $u)
                        <div id="editData_Delete{{ $u->id_template_dok }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/templatedok/delete{{ $u->id_template_dok }}" method="post">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Hapus Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="box-body">
                                            <input type="hidden" class="form-control" name="id_template_dok" value="{{ $u->id_template_dok }}">
                                            <p>Apakah Anda yakin akan menghapus data ini?</p>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach

                </div>
            </div>
        </div>
    </div>
</div>

		@if (count($errors) > 0)
            <div class="alert alert-danger">
	            <ul>
		            @foreach ($errors->all() as $error)
		            	<li>{{ $error }}</li>
		            @endforeach
	            </ul>
            </div>
        @endif

    <script type="text/javascript">
		$(document).ready(function() {
            $('#table_id').DataTable( {
                "scrollX": true
            } );
        } );
	</script>
@endsection
