@extends('layouts.app_custom')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-primary mb-3">
                    <h5 class="card-title">Approval RKAP Divisi {{ $dataupload->nama_divisi }}</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="table_id" class="table table-striped table-bordered nowrap" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama File RKAP</th>
                                        <th>Versi</th>
                                        <th>Opsi</th>
                                        <th>Tahun RKAP</th>
                                        <th>Tipe File</th>
                                        <th>Status RKAP</th>
                                        <th>Persetujuan EVP</th>
                                        <th>Catatan User RKAP</th>
                                        <th>Uploader RKAP</th>
                                        <th>Diperbarui oleh</th>
                                        <th>Approver RKAP</th>
                                        <th>Tanggal Diperbarui</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $n = 1?>
                                    @foreach($listupload as $p)
                                    <tr>
                                        <td>{{ $n++ }}</td>
                                        <td>{{ $p->file_dok }}</td>
                                        <td>
                                            @if($p->id_status_rkap == 1)
                                                0
                                            @else
                                                {{ $p->versi_dok }}
                                            @endif
                                        </td>
                                        <td>
                                            <center>
                                                @if($p->tipe_file_dok == 'docx')
                                                    <!-- <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#showData{{ $p->id_uploadrkapql }}">
                                                    <i class="fa fa-eye" aria-hidden="true"></i></button> -->
                                                    <a class="btn btn-primary" href="{{ $host.'/files/upload_dok/'.$p->file_dok }}" target="_blank">
                                                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                                    @if($p->id_status_rkap == 1
                                                    || $p->id_status_rkap == 4)
                                                        @if($p->is_approved_atasan == FALSE)
                                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                            data-target="#updateData{{ $p->id_uploadrkapql }}">
                                                            <i class="fa fa-upload" aria-hidden="true"></i> Approve</button>
                                                        @endif
                                                    @endif
                                                @elseif($p->tipe_file_dok == 'pptx')
                                                    <!-- <button type="button" class="btn btn-danger" data-toggle="modal"
                                                    data-target="#showData{{ $p->id_uploadrkapql }}">
                                                    <i class="fa fa-eye" aria-hidden="true"></i></button> -->
                                                    <a class="btn btn-danger" href="{{ $host.'/files/upload_dok/'.$p->file_dok }}" target="_blank">
                                                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                                    @if($p->id_status_rkap == 1
                                                    || $p->id_status_rkap == 4)
                                                        @if($p->is_approved_atasan == FALSE)
                                                            <button type="button" class="btn btn-danger" data-toggle="modal"
                                                            data-target="#updateData{{ $p->id_uploadrkapql }}">
                                                            <i class="fa fa-upload" aria-hidden="true"></i> Approve</button>
                                                        @endif
                                                    @endif
                                                @elseif($p->tipe_file_dok == 'xlsx')
                                                    <!-- <button type="button" class="btn btn-danger" data-toggle="modal"
                                                    data-target="#showData{{ $p->id_uploadrkapql }}">
                                                    <i class="fa fa-eye" aria-hidden="true"></i></button> -->
                                                    <a class="btn btn-success" href="{{ $host.'/files/upload_dok/'.$p->file_dok }}" target="_blank">
                                                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                                    @if($p->id_status_rkap == 1
                                                    || $p->id_status_rkap == 4)
                                                        @if($p->is_approved_atasan == FALSE)
                                                            <button type="button" class="btn btn-success" data-toggle="modal"
                                                            data-target="#updateData{{ $p->id_uploadrkapql }}">
                                                            <i class="fa fa-upload" aria-hidden="true"></i> Approve</button>
                                                        @endif
                                                    @endif
                                                @endif
                                            </center>
                                        </td>
                                        <td>{{ $p->tahun_dok }}</td>
                                        <td>{{ $p->tipe_file_dok }}</td>
                                        <td>
                                            <center>
                                                @if($p->id_status_rkap == 1)
                                                    <span class="badge badge-primary" style="background-color: #01579b;">Submitted</span>
                                                @elseif($p->id_status_rkap == 2)
                                                    <span class="badge badge-primary" style="background-color: #039be5;">Approved</span>
                                                @elseif($p->id_status_rkap == 3)
                                                    <span class="badge badge-primary" style="background-color: #f9a825;">Open for Revision</span>
                                                @elseif($p->id_status_rkap == 4)
                                                    <span class="badge badge-primary" style="background-color: #00bfa5;">Submitted (Revision)</span>
                                                @endif
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                @if($p->is_approved_atasan == TRUE)
                                                    <i class="fa fa-check fa-2x" aria-hidden="true" style="color: green;"></i>
                                                @elseif($p->is_approved_atasan == FALSE)
                                                    <i class="fa fa-times fa-2x" aria-hidden="true" style="color: red;"></i>
                                                @endif
                                            </center>
                                        </td>
                                        <td>{{ $p->catatan_upload }}</td>
                                        <td>{{ $p->nama_uploader }}</td>
                                        <td>{{ $p->last_updated_by }}</td>
                                        <td>{{ $p->nama_approver }}</td>
                                        <td>{{ $p->last_updated_on }}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>

                        @foreach($listupload as $u)
                        <div id="showData{{ $u->id_uploadrkapql }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-xl modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Preview File RKAP</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <iframe src="https://view.officeapps.live.com/op/view.aspx?src={{url('https://file-examples.com/wp-content/uploads/2017/02/file-sample_100kB.doc')}}" frameborder="0" style="width: 100%; min-height: 640px;"></iframe>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        @foreach($listupload as $u)
                        <div id="updateData{{ $u->id_uploadrkapql }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                <form action="/approve_submit{{ $u->id_uploadrkapql }}" method="post">

                                {{ csrf_field() }}

                                <div class="modal-header">
                                    <h4 class="modal-title">Approval Divisi</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <div class="box-body">
                                        <input type="hidden" class="form-control" name="id_uploadrkapql" value="{{ $u->id_uploadrkapql }}">
                                        <h5 style="color: red;">*Mohon Dilakukan Download dan Pengecekan File RKAP sebelum disetujui</h5>
                                        <p>Apakah Anda yakin akan menyetujui file RKAP Divisi {{ $dataupload->nama_divisi }} ini?</p>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Approve</button>
                                </div>
                                </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

		@if (count($errors) > 0)
            <div class="alert alert-danger">
	            <ul>
		            @foreach ($errors->all() as $error)
		            	<li>{{ $error }}</li>
		            @endforeach
	            </ul>
            </div>
        @endif

    <script type="text/javascript">
		$(document).ready(function() {
            $('#table_id').DataTable( {
                "scrollX": true
            } );
        } );
	</script>
@endsection