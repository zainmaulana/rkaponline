@extends('layouts.app_custom')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-primary mb-3">
                    <h5 class="card-title">Monitoring RKAP Divisi {{ $dataupload->nama_divisi }}</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="table_id" class="table table-striped table-bordered nowrap" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama File RKAP</th>
                                        <th>Versi</th>
                                        <th>Opsi</th>
                                        <th>Tahun RKAP</th>
                                        <th>Tipe File</th>
                                        <th>Status RKAP</th>
                                        <th>Persetujuan EVP</th>
                                        <th>Catatan User RKAP</th>
                                        <th>Uploader RKAP</th>
                                        <th>Diperbarui oleh</th>
                                        <th>Approver RKAP</th>
                                        <th>Tanggal Diperbarui</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $n = 1?>
                                    @foreach($listupload as $p)
                                    <tr>
                                        <td>{{ $n++ }}</td>
                                        <td>{{ $p->file_dok }}</td>
                                        <td>
                                            @if($p->id_status_rkap == 1)
                                                0
                                            @else
                                                {{ $p->versi_dok }}
                                            @endif
                                        </td>
                                        <td>
                                            <center>
                                                @if($p->tipe_file_dok == 'docx')
                                                    @if($p->versi_dok > 0)
                                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#showLog{{ $p->id_uploadrkapql }}">
                                                        <i class="fa fa-sticky-note-o" aria-hidden="true"></i> Log File</button>
                                                    @endif
                                                    <!-- <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#showData{{ $p->id_uploadrkapql }}">
                                                    <i class="fa fa-eye" aria-hidden="true"></i></button> -->
                                                    <a class="btn btn-primary" href="{{ $host.'/files/upload_dok/'.$p->file_dok }}" target="_blank">
                                                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                                    @if($p->id_status_rkap == 3)
                                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#updateData{{ $p->id_uploadrkapql }}">
                                                        <i class="fa fa-upload" aria-hidden="true"></i> Revisi</button>
                                                    @endif
                                                @elseif($p->tipe_file_dok == 'pptx')
                                                    @if($p->versi_dok > 0)
                                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                                        data-target="#showLog{{ $p->id_uploadrkapql }}">
                                                        <i class="fa fa-sticky-note-o" aria-hidden="true"></i> Log File</button>
                                                    @endif
                                                    <!-- <button type="button" class="btn btn-danger" data-toggle="modal"
                                                    data-target="#showData{{ $p->id_uploadrkapql }}">
                                                    <i class="fa fa-eye" aria-hidden="true"></i></button> -->
                                                    <a class="btn btn-danger" href="{{ $host.'/files/upload_dok/'.$p->file_dok }}" target="_blank">
                                                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                                    @if($p->id_status_rkap == 3)
                                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                                        data-target="#updateData{{ $p->id_uploadrkapql }}">
                                                        <i class="fa fa-upload" aria-hidden="true"></i> Revisi</button>
                                                    @endif
                                                @elseif($p->tipe_file_dok == 'xlsx')
                                                    @if($p->versi_dok > 0)
                                                        <button type="button" class="btn btn-success" data-toggle="modal"
                                                        data-target="#showLog{{ $p->id_uploadrkapql }}">
                                                        <i class="fa fa-sticky-note-o" aria-hidden="true"></i> Log File</button>
                                                    @endif
                                                    <!-- <button type="button" class="btn btn-danger" data-toggle="modal"
                                                    data-target="#showData{{ $p->id_uploadrkapql }}">
                                                    <i class="fa fa-eye" aria-hidden="true"></i></button> -->
                                                    <a class="btn btn-success" href="{{ $host.'/files/upload_dok/'.$p->file_dok }}" target="_blank">
                                                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                                    @if($p->id_status_rkap == 3)
                                                        <button type="button" class="btn btn-success" data-toggle="modal"
                                                        data-target="#updateData{{ $p->id_uploadrkapql }}">
                                                        <i class="fa fa-upload" aria-hidden="true"></i> Revisi</button>
                                                    @endif
                                                @endif
                                            </center>
                                        </td>
                                        <td>{{ $p->tahun_dok }}</td>
                                        <td>{{ $p->tipe_file_dok }}</td>
                                        <td>
                                            <center>
                                                @if($p->id_status_rkap == 1)
                                                    <span class="badge badge-primary" style="background-color: #01579b;">Submitted</span>
                                                @elseif($p->id_status_rkap == 2)
                                                    <span class="badge badge-primary" style="background-color: #039be5;">Approved</span>
                                                @elseif($p->id_status_rkap == 3)
                                                    <span class="badge badge-primary" style="background-color: #f9a825;">Open for Revision</span>
                                                @elseif($p->id_status_rkap == 4)
                                                    <span class="badge badge-primary" style="background-color: #00bfa5;">Submitted (Revision)</span>
                                                @endif
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                @if($p->is_approved_atasan == TRUE)
                                                    <i class="fa fa-check fa-2x" aria-hidden="true" style="color: green;"></i>
                                                @elseif($p->is_approved_atasan == FALSE)
                                                    <i class="fa fa-times fa-2x" aria-hidden="true" style="color: red;"></i>
                                                @endif
                                            </center>
                                        </td>
                                        <td>{{ $p->catatan_upload }}</td>
                                        <td>{{ $p->nama_uploader }}</td>
                                        <td>{{ $p->last_updated_by }}</td>
                                        <td>{{ $p->nama_approver }}</td>
                                        <td>{{ $p->last_updated_on }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                        @foreach($listupload as $u)
                        <div id="showLog{{ $u->id_uploadrkapql }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-xl modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Log File {{ $u->file_dok }}</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-striped table-bordered nowrap" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Nama File RKAP</th>
                                                    <th>Versi</th>
                                                    <th>Opsi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $listuploadlog = DB::table('tblt_uploadrkapql as a')
                                                    ->leftJoin('tblt_uploadrkapql_history as b','a.id_uploadrkapql','=','b.id_uploadrkapql')
                                                    ->select('a.id_uploadrkapql',
                                                    'a.id_status_rkap',
                                                    'a.tipe_file_dok',
                                                    'b.*')
                                                    ->where('a.id_uploadrkapql','=',$u->id_uploadrkapql)
                                                    ->orderBy('b.versi_file_dok','desc')
                                                    ->get();

                                                    $n = 1;
                                                ?>
                                                @foreach($listuploadlog as $p)
                                                <tr>
                                                    <td>{{ $n++ }}</td>
                                                    <td>{{ $p->file_dok }}</td>
                                                    <td>
                                                        @if($p->id_status_rkap == 1)
                                                            0
                                                        @else
                                                            {{ $p->versi_file_dok }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <center>
                                                            @if($p->tipe_file_dok == 'docx')
                                                                <a class="btn btn-primary" href="{{ $host.'/files/upload_dok/'.$p->file_dok }}" target="_blank">
                                                                <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                                            @elseif($p->tipe_file_dok == 'pptx')
                                                                <a class="btn btn-danger" href="{{ $host.'/files/upload_dok/'.$p->file_dok }}" target="_blank">
                                                                <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                                            @elseif($p->tipe_file_dok == 'xlsx')
                                                                <a class="btn btn-success" href="{{ $host.'/files/upload_dok/'.$p->file_dok }}" target="_blank">
                                                                <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                                            @endif
                                                        </center>
                                                    </td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        @foreach($listupload as $u)
                        <div id="showData{{ $u->id_uploadrkapql }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-xl modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Preview File RKAP</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <iframe src="https://view.officeapps.live.com/op/view.aspx?src={{url('https://file-examples.com/wp-content/uploads/2017/02/file-sample_100kB.doc')}}" frameborder="0" style="width: 100%; min-height: 640px;"></iframe>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        @foreach($listupload as $u)
                        <div id="updateData{{ $u->id_uploadrkapql }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Revisi {{ $u->file_dok }}</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="/upload_revision{{ $u->id_uploadrkapql }}" method="post" enctype="multipart/form-data">

                                            {{ csrf_field() }}

                                            <div class="form-group">
                                                <label for="file_attach">Upload File RKAP</label>
                                                    <div class="custom-file" id="file_attach">
                                                        <input type="file" class="custom-file-input" id="customFile" name="file_dok" required>
                                                        <label class="custom-file-label" for="customFile">Pilih File</label>
                                                    </div>
                                                    <input type="hidden" name="id_uploadrkapql" value="{{ $u->id_uploadrkapql }}">
                                                    <input type="hidden" name="nama_divisi" value="{{ $u->nama_divisi }}">
                                                    <input type="hidden" name="id_divisi" value="{{ $dataupload->id_divisi }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="catatan_upload">Catatan Upload File RKAP</label>
                                                <textarea class="form-control" id="catatan_upload" name="catatan_upload" rows="2" required></textarea>
                                            </div>

                                            @if($schclosing)
                                                <input type="hidden" name="waktu_closing" value="{{ $schclosing->waktu_closing }}">
                                            @endif

                                            <div class="d-flex justify-content-center">
                                                @if($schclosing)
                                                    @if($schclosing->waktu_closing > $time_now)
                                                        <button type="submit" class="btn btn-primary" style="width: 18rem;">Submit</button>
                                                    @else
                                                        <button type="submit" class="btn btn-primary" style="width: 18rem;" disabled>Closed</button>
                                                    @endif
                                                @else
                                                    Waktu Closing Belum Ada
                                                @endif
                                            </div>

                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

		@if (count($errors) > 0)
            <div class="alert alert-danger">
	            <ul>
		            @foreach ($errors->all() as $error)
		            	<li>{{ $error }}</li>
		            @endforeach
	            </ul>
            </div>
        @endif

    <script type="text/javascript">
		$(document).ready(function() {
            $('#table_id').DataTable( {
                "scrollX": true
            } );
        } );

        $('#customFile').on('change',function(){
			//get the file name
			var fileName = $(this).val().replace('C:\\fakepath\\', " ");
			//replace the "Choose a file" label
			$(this).next('.custom-file-label').html(fileName);
		})
	</script>
@endsection