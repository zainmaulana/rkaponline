@extends('layouts.app_custom')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-primary mb-3">
                    <h5 class="card-title">Template RKAP {{ $dataupload->nama_divisi }}</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="table_id" class="table table-striped table-bordered nowrap" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Template RKAP</th>
                                        <th>Jenis RKAP</th>
                                        <th>Jenis File</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $n = 1?>
                                    @foreach($downloadform as $p)
                                    <tr>
                                        <td>{{ $n++ }}</td>
                                        <td>{{ $p->nama_template_dok }}</td>
                                        <td>{{ $p->jenis_rkap }}</td>
                                        <td>
                                        @if($p->tipe_file_dok == 'xlsx')
                                            <span class="badge badge-success">Excel</span>
                                        @elseif($p->tipe_file_dok == 'docx')
                                            <span class="badge badge-primary">Word</span>
                                        @elseif($p->tipe_file_dok == 'pptx')
                                            <span class="badge badge-danger">Power Point</span>
                                        @endif
                                        </td>
                                        <td>
                                        @if($p->tipe_file_dok == 'xlsx')
                                            <center>
                                                <a class="btn btn-success" href="{{ $host.'/files/template_dok/'.$p->file_dok }}" target="_blank">
                                                <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                            </center>
                                        @elseif($p->tipe_file_dok == 'docx')
                                            <center>
                                                <a class="btn btn-primary" href="{{ $host.'/files/template_dok/'.$p->file_dok }}" target="_blank">
                                                <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                            </center>
                                        @elseif($p->tipe_file_dok == 'pptx')
                                            <center>
                                                <a class="btn btn-danger" href="{{ $host.'/files/template_dok/'.$p->file_dok }}" target="_blank">
                                                <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download</a>
                                            </center>
                                        @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

		@if (count($errors) > 0)
            <div class="alert alert-danger">
	            <ul>
		            @foreach ($errors->all() as $error)
		            	<li>{{ $error }}</li>
		            @endforeach
	            </ul>
            </div>
        @endif

    <script type="text/javascript">
		$(document).ready(function() {
            $('#table_id').DataTable( {
                "scrollX": true
            } );
        } );
	</script>
@endsection