--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.19
-- Dumped by pg_dump version 11.5

-- Started on 2021-07-09 18:32:22

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 187 (class 1259 OID 44500)
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 44498)
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- TOC entry 2231 (class 0 OID 0)
-- Dependencies: 186
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- TOC entry 182 (class 1259 OID 44472)
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 44470)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2232 (class 0 OID 0)
-- Dependencies: 181
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 192 (class 1259 OID 44536)
-- Name: model_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_permissions (
    permission_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_permissions OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 44547)
-- Name: model_has_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_roles (
    role_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_roles OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 44491)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 44512)
-- Name: permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    id_divisi bigint,
    nama_menu text,
    nama_sub_menu text
);


ALTER TABLE public.permissions OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 44510)
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO postgres;

--
-- TOC entry 2233 (class 0 OID 0)
-- Dependencies: 188
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- TOC entry 194 (class 1259 OID 44558)
-- Name: role_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role_has_permissions (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.role_has_permissions OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 44525)
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 44523)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- TOC entry 2234 (class 0 OID 0)
-- Dependencies: 190
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- TOC entry 196 (class 1259 OID 44575)
-- Name: tblm_divisi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tblm_divisi (
    id_divisi bigint NOT NULL,
    nama_divisi text,
    kode_divisi text,
    lantai_divisi integer,
    image_divisi text,
    url_divisi text,
    no_urut_divisi integer
);


ALTER TABLE public.tblm_divisi OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 44573)
-- Name: tblm_divisi_id_divisi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tblm_divisi_id_divisi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tblm_divisi_id_divisi_seq OWNER TO postgres;

--
-- TOC entry 2235 (class 0 OID 0)
-- Dependencies: 195
-- Name: tblm_divisi_id_divisi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tblm_divisi_id_divisi_seq OWNED BY public.tblm_divisi.id_divisi;


--
-- TOC entry 200 (class 1259 OID 44598)
-- Name: tblm_scheduleclosing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tblm_scheduleclosing (
    id_scheduleclosing bigint NOT NULL,
    id_divisi bigint,
    jenis_rkap text,
    tahun_dok integer,
    waktu_closing timestamp without time zone,
    created_on timestamp without time zone,
    last_updated_on timestamp without time zone
);


ALTER TABLE public.tblm_scheduleclosing OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 44596)
-- Name: tblm_scheduleclosing_id_scheduleclosing_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tblm_scheduleclosing_id_scheduleclosing_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tblm_scheduleclosing_id_scheduleclosing_seq OWNER TO postgres;

--
-- TOC entry 2236 (class 0 OID 0)
-- Dependencies: 199
-- Name: tblm_scheduleclosing_id_scheduleclosing_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tblm_scheduleclosing_id_scheduleclosing_seq OWNED BY public.tblm_scheduleclosing.id_scheduleclosing;


--
-- TOC entry 198 (class 1259 OID 44587)
-- Name: tblm_templatedokumen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tblm_templatedokumen (
    id_template_dok bigint NOT NULL,
    id_divisi bigint,
    nama_template_dok text,
    jenis_rkap text,
    tahun_dok integer,
    tipe_file_dok text,
    file_dok text,
    created_on timestamp without time zone,
    last_updated_on timestamp without time zone
);


ALTER TABLE public.tblm_templatedokumen OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 44585)
-- Name: tblm_templatedokumen_id_template_dok_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tblm_templatedokumen_id_template_dok_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tblm_templatedokumen_id_template_dok_seq OWNER TO postgres;

--
-- TOC entry 2237 (class 0 OID 0)
-- Dependencies: 197
-- Name: tblm_templatedokumen_id_template_dok_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tblm_templatedokumen_id_template_dok_seq OWNED BY public.tblm_templatedokumen.id_template_dok;


--
-- TOC entry 184 (class 1259 OID 44480)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    id_role bigint,
    id_divisi bigint
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 44478)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2238 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2049 (class 2604 OID 44503)
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- TOC entry 2047 (class 2604 OID 44475)
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 2051 (class 2604 OID 44515)
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- TOC entry 2052 (class 2604 OID 44528)
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- TOC entry 2053 (class 2604 OID 44578)
-- Name: tblm_divisi id_divisi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_divisi ALTER COLUMN id_divisi SET DEFAULT nextval('public.tblm_divisi_id_divisi_seq'::regclass);


--
-- TOC entry 2055 (class 2604 OID 44601)
-- Name: tblm_scheduleclosing id_scheduleclosing; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_scheduleclosing ALTER COLUMN id_scheduleclosing SET DEFAULT nextval('public.tblm_scheduleclosing_id_scheduleclosing_seq'::regclass);


--
-- TOC entry 2054 (class 2604 OID 44590)
-- Name: tblm_templatedokumen id_template_dok; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_templatedokumen ALTER COLUMN id_template_dok SET DEFAULT nextval('public.tblm_templatedokumen_id_template_dok_seq'::regclass);


--
-- TOC entry 2048 (class 2604 OID 44483)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2211 (class 0 OID 44500)
-- Dependencies: 187
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.failed_jobs (id, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- TOC entry 2206 (class 0 OID 44472)
-- Dependencies: 182
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_08_19_000000_create_failed_jobs_table	1
4	2021_07_06_040854_create_permission_tables	2
\.


--
-- TOC entry 2216 (class 0 OID 44536)
-- Dependencies: 192
-- Data for Name: model_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.model_has_permissions (permission_id, model_type, model_id) FROM stdin;
\.


--
-- TOC entry 2217 (class 0 OID 44547)
-- Dependencies: 193
-- Data for Name: model_has_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.model_has_roles (role_id, model_type, model_id) FROM stdin;
1	App\\User	1
\.


--
-- TOC entry 2209 (class 0 OID 44491)
-- Dependencies: 185
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- TOC entry 2213 (class 0 OID 44512)
-- Dependencies: 189
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.permissions (id, name, guard_name, created_at, updated_at, id_divisi, nama_menu, nama_sub_menu) FROM stdin;
1	01_menu	web	2021-07-08 05:01:39	2021-07-08 05:05:32	1	Master Data	\N
\.


--
-- TOC entry 2218 (class 0 OID 44558)
-- Dependencies: 194
-- Data for Name: role_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role_has_permissions (permission_id, role_id) FROM stdin;
1	1
\.


--
-- TOC entry 2215 (class 0 OID 44525)
-- Dependencies: 191
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name, guard_name, created_at, updated_at) FROM stdin;
1	Super Admin	web	2021-07-08 05:05:47	2021-07-08 05:05:47
\.


--
-- TOC entry 2220 (class 0 OID 44575)
-- Dependencies: 196
-- Data for Name: tblm_divisi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tblm_divisi (id_divisi, nama_divisi, kode_divisi, lantai_divisi, image_divisi, url_divisi, no_urut_divisi) FROM stdin;
1	Sistem, TI & Riset Teknologi	SIT	8	\N	\N	1
\.


--
-- TOC entry 2224 (class 0 OID 44598)
-- Dependencies: 200
-- Data for Name: tblm_scheduleclosing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tblm_scheduleclosing (id_scheduleclosing, id_divisi, jenis_rkap, tahun_dok, waktu_closing, created_on, last_updated_on) FROM stdin;
1	\N	Kualitatif	2021	2021-07-16 23:30:00	2021-07-09 11:11:08	2021-07-09 18:23:40
\.


--
-- TOC entry 2222 (class 0 OID 44587)
-- Dependencies: 198
-- Data for Name: tblm_templatedokumen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tblm_templatedokumen (id_template_dok, id_divisi, nama_template_dok, jenis_rkap, tahun_dok, tipe_file_dok, file_dok, created_on, last_updated_on) FROM stdin;
6	1	RKAP_SIT	Kuantitatif	2022	xlsx	RKAP_SIT_2021.xlsx	2021-07-09 09:26:11	2021-07-09 16:26:21
\.


--
-- TOC entry 2208 (class 0 OID 44480)
-- Dependencies: 184
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, id_role, id_divisi) FROM stdin;
1	Zain Maulana Azmi	zain.maulana@hutamakarya.com	\N	$2y$10$PE3WnS2Gx./ZJCijAYuKE.8nPTuloeuRx9KVuUqYsImgxNoZW0A92	\N	2021-07-07 02:33:35	2021-07-08 05:07:26	1	1
\.


--
-- TOC entry 2239 (class 0 OID 0)
-- Dependencies: 186
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- TOC entry 2240 (class 0 OID 0)
-- Dependencies: 181
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 4, true);


--
-- TOC entry 2241 (class 0 OID 0)
-- Dependencies: 188
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.permissions_id_seq', 1, true);


--
-- TOC entry 2242 (class 0 OID 0)
-- Dependencies: 190
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 1, true);


--
-- TOC entry 2243 (class 0 OID 0)
-- Dependencies: 195
-- Name: tblm_divisi_id_divisi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tblm_divisi_id_divisi_seq', 1, true);


--
-- TOC entry 2244 (class 0 OID 0)
-- Dependencies: 199
-- Name: tblm_scheduleclosing_id_scheduleclosing_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tblm_scheduleclosing_id_scheduleclosing_seq', 1, true);


--
-- TOC entry 2245 (class 0 OID 0)
-- Dependencies: 197
-- Name: tblm_templatedokumen_id_template_dok_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tblm_templatedokumen_id_template_dok_seq', 6, true);


--
-- TOC entry 2246 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- TOC entry 2064 (class 2606 OID 44509)
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- TOC entry 2057 (class 2606 OID 44477)
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2075 (class 2606 OID 44546)
-- Name: model_has_permissions model_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);


--
-- TOC entry 2078 (class 2606 OID 44557)
-- Name: model_has_roles model_has_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);


--
-- TOC entry 2066 (class 2606 OID 44522)
-- Name: permissions permissions_name_guard_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_name_guard_name_unique UNIQUE (name, guard_name);


--
-- TOC entry 2068 (class 2606 OID 44520)
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2080 (class 2606 OID 44572)
-- Name: role_has_permissions role_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);


--
-- TOC entry 2070 (class 2606 OID 44535)
-- Name: roles roles_name_guard_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_name_guard_name_unique UNIQUE (name, guard_name);


--
-- TOC entry 2072 (class 2606 OID 44533)
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2082 (class 2606 OID 44583)
-- Name: tblm_divisi tblm_divisi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_divisi
    ADD CONSTRAINT tblm_divisi_pkey PRIMARY KEY (id_divisi);


--
-- TOC entry 2086 (class 2606 OID 44606)
-- Name: tblm_scheduleclosing tblm_scheduleclosing_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_scheduleclosing
    ADD CONSTRAINT tblm_scheduleclosing_pkey PRIMARY KEY (id_scheduleclosing);


--
-- TOC entry 2084 (class 2606 OID 44595)
-- Name: tblm_templatedokumen tblm_templatedokumen_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_templatedokumen
    ADD CONSTRAINT tblm_templatedokumen_pkey PRIMARY KEY (id_template_dok);


--
-- TOC entry 2059 (class 2606 OID 44490)
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 2061 (class 2606 OID 44488)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2073 (class 1259 OID 44539)
-- Name: model_has_permissions_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_permissions_model_id_model_type_index ON public.model_has_permissions USING btree (model_id, model_type);


--
-- TOC entry 2076 (class 1259 OID 44550)
-- Name: model_has_roles_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_roles_model_id_model_type_index ON public.model_has_roles USING btree (model_id, model_type);


--
-- TOC entry 2062 (class 1259 OID 44497)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- TOC entry 2087 (class 2606 OID 44540)
-- Name: model_has_permissions model_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- TOC entry 2088 (class 2606 OID 44551)
-- Name: model_has_roles model_has_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- TOC entry 2089 (class 2606 OID 44561)
-- Name: role_has_permissions role_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- TOC entry 2090 (class 2606 OID 44566)
-- Name: role_has_permissions role_has_permissions_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- TOC entry 2230 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2021-07-09 18:32:23

--
-- PostgreSQL database dump complete
--

