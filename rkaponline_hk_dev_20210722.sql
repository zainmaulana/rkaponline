--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.19
-- Dumped by pg_dump version 11.5

-- Started on 2021-07-22 16:25:43

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 202 (class 1259 OID 44609)
-- Name: activity_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.activity_log (
    id bigint NOT NULL,
    log_name character varying(255),
    description text NOT NULL,
    subject_type character varying(255),
    subject_id bigint,
    causer_type character varying(255),
    causer_id bigint,
    properties json,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.activity_log OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 44607)
-- Name: activity_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.activity_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activity_log_id_seq OWNER TO postgres;

--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 201
-- Name: activity_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.activity_log_id_seq OWNED BY public.activity_log.id;


--
-- TOC entry 187 (class 1259 OID 44500)
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 44498)
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 186
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- TOC entry 182 (class 1259 OID 44472)
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 44470)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 181
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 192 (class 1259 OID 44536)
-- Name: model_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_permissions (
    permission_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_permissions OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 44547)
-- Name: model_has_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_roles (
    role_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_roles OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 44491)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 44512)
-- Name: permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    id_divisi bigint,
    nama_menu text,
    nama_sub_menu text
);


ALTER TABLE public.permissions OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 44510)
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO postgres;

--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 188
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- TOC entry 194 (class 1259 OID 44558)
-- Name: role_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role_has_permissions (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.role_has_permissions OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 44525)
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 44523)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 190
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- TOC entry 196 (class 1259 OID 44575)
-- Name: tblm_divisi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tblm_divisi (
    id_divisi bigint NOT NULL,
    nama_divisi text,
    kode_divisi text,
    lantai_divisi integer,
    image_divisi text,
    url_divisi text,
    no_urut_divisi integer
);


ALTER TABLE public.tblm_divisi OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 44573)
-- Name: tblm_divisi_id_divisi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tblm_divisi_id_divisi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tblm_divisi_id_divisi_seq OWNER TO postgres;

--
-- TOC entry 2275 (class 0 OID 0)
-- Dependencies: 195
-- Name: tblm_divisi_id_divisi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tblm_divisi_id_divisi_seq OWNED BY public.tblm_divisi.id_divisi;


--
-- TOC entry 200 (class 1259 OID 44598)
-- Name: tblm_scheduleclosing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tblm_scheduleclosing (
    id_scheduleclosing bigint NOT NULL,
    id_divisi bigint,
    jenis_rkap text,
    tahun_dok integer,
    waktu_closing timestamp without time zone,
    created_on timestamp without time zone,
    last_updated_on timestamp without time zone
);


ALTER TABLE public.tblm_scheduleclosing OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 44596)
-- Name: tblm_scheduleclosing_id_scheduleclosing_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tblm_scheduleclosing_id_scheduleclosing_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tblm_scheduleclosing_id_scheduleclosing_seq OWNER TO postgres;

--
-- TOC entry 2276 (class 0 OID 0)
-- Dependencies: 199
-- Name: tblm_scheduleclosing_id_scheduleclosing_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tblm_scheduleclosing_id_scheduleclosing_seq OWNED BY public.tblm_scheduleclosing.id_scheduleclosing;


--
-- TOC entry 198 (class 1259 OID 44587)
-- Name: tblm_templatedokumen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tblm_templatedokumen (
    id_template_dok bigint NOT NULL,
    id_divisi bigint,
    nama_template_dok text,
    jenis_rkap text,
    tahun_dok integer,
    tipe_file_dok text,
    file_dok text,
    created_on timestamp without time zone,
    last_updated_on timestamp without time zone
);


ALTER TABLE public.tblm_templatedokumen OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 44585)
-- Name: tblm_templatedokumen_id_template_dok_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tblm_templatedokumen_id_template_dok_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tblm_templatedokumen_id_template_dok_seq OWNER TO postgres;

--
-- TOC entry 2277 (class 0 OID 0)
-- Dependencies: 197
-- Name: tblm_templatedokumen_id_template_dok_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tblm_templatedokumen_id_template_dok_seq OWNED BY public.tblm_templatedokumen.id_template_dok;


--
-- TOC entry 206 (class 1259 OID 45291)
-- Name: tblt_uploadrkapql; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tblt_uploadrkapql (
    id_uploadrkapql bigint NOT NULL,
    id_user_upload bigint NOT NULL,
    id_status_rkap bigint NOT NULL,
    id_user_approver bigint,
    tahun_dok integer NOT NULL,
    tipe_file_dok text,
    file_dok text,
    catatan_upload text,
    is_approved_atasan boolean,
    created_on timestamp without time zone,
    last_updated_by text,
    last_updated_on timestamp without time zone
);


ALTER TABLE public.tblt_uploadrkapql OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 44664)
-- Name: tblt_uploadrkapql_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tblt_uploadrkapql_history (
    id_uploadrkapql_history bigint NOT NULL,
    id_uploadrkapql bigint NOT NULL,
    file_dok text,
    versi_file_dok integer,
    created_on timestamp without time zone
);


ALTER TABLE public.tblt_uploadrkapql_history OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 44662)
-- Name: tblt_uploadrkapql_history_id_uploadrkapql_history_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tblt_uploadrkapql_history_id_uploadrkapql_history_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tblt_uploadrkapql_history_id_uploadrkapql_history_seq OWNER TO postgres;

--
-- TOC entry 2278 (class 0 OID 0)
-- Dependencies: 203
-- Name: tblt_uploadrkapql_history_id_uploadrkapql_history_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tblt_uploadrkapql_history_id_uploadrkapql_history_seq OWNED BY public.tblt_uploadrkapql_history.id_uploadrkapql_history;


--
-- TOC entry 205 (class 1259 OID 45289)
-- Name: tblt_uploadrkapql_id_uploadrkapql_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tblt_uploadrkapql_id_uploadrkapql_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tblt_uploadrkapql_id_uploadrkapql_seq OWNER TO postgres;

--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 205
-- Name: tblt_uploadrkapql_id_uploadrkapql_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tblt_uploadrkapql_id_uploadrkapql_seq OWNED BY public.tblt_uploadrkapql.id_uploadrkapql;


--
-- TOC entry 184 (class 1259 OID 44480)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    id_role bigint,
    id_divisi bigint
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 44478)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2077 (class 2604 OID 44612)
-- Name: activity_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activity_log ALTER COLUMN id SET DEFAULT nextval('public.activity_log_id_seq'::regclass);


--
-- TOC entry 2070 (class 2604 OID 44503)
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- TOC entry 2068 (class 2604 OID 44475)
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 2072 (class 2604 OID 44515)
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- TOC entry 2073 (class 2604 OID 44528)
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- TOC entry 2074 (class 2604 OID 44578)
-- Name: tblm_divisi id_divisi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_divisi ALTER COLUMN id_divisi SET DEFAULT nextval('public.tblm_divisi_id_divisi_seq'::regclass);


--
-- TOC entry 2076 (class 2604 OID 44601)
-- Name: tblm_scheduleclosing id_scheduleclosing; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_scheduleclosing ALTER COLUMN id_scheduleclosing SET DEFAULT nextval('public.tblm_scheduleclosing_id_scheduleclosing_seq'::regclass);


--
-- TOC entry 2075 (class 2604 OID 44590)
-- Name: tblm_templatedokumen id_template_dok; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_templatedokumen ALTER COLUMN id_template_dok SET DEFAULT nextval('public.tblm_templatedokumen_id_template_dok_seq'::regclass);


--
-- TOC entry 2079 (class 2604 OID 45294)
-- Name: tblt_uploadrkapql id_uploadrkapql; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblt_uploadrkapql ALTER COLUMN id_uploadrkapql SET DEFAULT nextval('public.tblt_uploadrkapql_id_uploadrkapql_seq'::regclass);


--
-- TOC entry 2078 (class 2604 OID 44667)
-- Name: tblt_uploadrkapql_history id_uploadrkapql_history; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblt_uploadrkapql_history ALTER COLUMN id_uploadrkapql_history SET DEFAULT nextval('public.tblt_uploadrkapql_history_id_uploadrkapql_history_seq'::regclass);


--
-- TOC entry 2069 (class 2604 OID 44483)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2259 (class 0 OID 44609)
-- Dependencies: 202
-- Data for Name: activity_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.activity_log (id, log_name, description, subject_type, subject_id, causer_type, causer_id, properties, created_at, updated_at) FROM stdin;
107	default	Corporate Planning was login	App\\User	\N	App\\User	2	[]	2021-07-22 08:23:52	2021-07-22 08:23:52
108	default	EVP SIT was login	App\\User	\N	App\\User	3	[]	2021-07-22 08:24:18	2021-07-22 08:24:18
109	default	created	App\\TransaksiUpload	3	App\\User	1	{"attributes":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.docx","catatan_upload":"Upload Pertama","is_approved_atasan":false,"last_updated_by":null}}	2021-07-22 08:43:30	2021-07-22 08:43:30
110	default	updated	App\\TransaksiUpload	3	App\\User	1	{"attributes":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.docx","catatan_upload":"Upload Kedua","is_approved_atasan":false,"last_updated_by":"Zain Maulana Azmi"},"old":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.docx","catatan_upload":"Upload Pertama","is_approved_atasan":false,"last_updated_by":null}}	2021-07-22 08:46:18	2021-07-22 08:46:18
111	default	updated	App\\TransaksiUpload	3	App\\User	3	{"attributes":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.docx","catatan_upload":"Upload Kedua","is_approved_atasan":true,"last_updated_by":"Zain Maulana Azmi"},"old":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.docx","catatan_upload":"Upload Kedua","is_approved_atasan":false,"last_updated_by":"Zain Maulana Azmi"}}	2021-07-22 08:55:29	2021-07-22 08:55:29
112	default	created	App\\TransaksiUpload	4	App\\User	1	{"attributes":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"pptx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.pptx","catatan_upload":"Upload PPT","is_approved_atasan":false,"last_updated_by":null}}	2021-07-22 08:56:12	2021-07-22 08:56:12
113	default	updated	App\\TransaksiUpload	3	App\\User	2	{"attributes":{"id_user_upload":1,"id_status_rkap":2,"id_user_approver":2,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.docx","catatan_upload":"Upload Kedua","is_approved_atasan":true,"last_updated_by":"Zain Maulana Azmi"},"old":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.docx","catatan_upload":"Upload Kedua","is_approved_atasan":true,"last_updated_by":"Zain Maulana Azmi"}}	2021-07-22 08:56:42	2021-07-22 08:56:42
114	default	created	App\\TransaksiUploadHistory	10	App\\User	2	{"attributes":{"id_uploadrkapql":3,"file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.docx","versi_file_dok":1}}	2021-07-22 08:56:42	2021-07-22 08:56:42
115	default	updated	App\\TransaksiUpload	4	App\\User	1	{"attributes":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"pptx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.pptx","catatan_upload":"Upload PPT Kedua","is_approved_atasan":false,"last_updated_by":"Zain Maulana Azmi"},"old":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"pptx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.pptx","catatan_upload":"Upload PPT","is_approved_atasan":false,"last_updated_by":null}}	2021-07-22 08:58:40	2021-07-22 08:58:40
116	default	updated	App\\TransaksiUpload	3	App\\User	2	{"attributes":{"id_user_upload":1,"id_status_rkap":3,"id_user_approver":2,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.docx","catatan_upload":"Upload Kedua","is_approved_atasan":false,"last_updated_by":"Zain Maulana Azmi"},"old":{"id_user_upload":1,"id_status_rkap":2,"id_user_approver":2,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.docx","catatan_upload":"Upload Kedua","is_approved_atasan":true,"last_updated_by":"Zain Maulana Azmi"}}	2021-07-22 08:58:58	2021-07-22 08:58:58
117	default	updated	App\\TransaksiUpload	3	App\\User	1	{"attributes":{"id_user_upload":1,"id_status_rkap":4,"id_user_approver":2,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi_v2.docx","catatan_upload":"Ada Revisi di Bagian TI","is_approved_atasan":false,"last_updated_by":"Zain Maulana Azmi"},"old":{"id_user_upload":1,"id_status_rkap":3,"id_user_approver":2,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.docx","catatan_upload":"Upload Kedua","is_approved_atasan":false,"last_updated_by":"Zain Maulana Azmi"}}	2021-07-22 09:03:49	2021-07-22 09:03:49
118	default	updated	App\\TransaksiUpload	3	App\\User	3	{"attributes":{"id_user_upload":1,"id_status_rkap":4,"id_user_approver":2,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi_v2.docx","catatan_upload":"Ada Revisi di Bagian TI","is_approved_atasan":true,"last_updated_by":"Zain Maulana Azmi"},"old":{"id_user_upload":1,"id_status_rkap":4,"id_user_approver":2,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi_v2.docx","catatan_upload":"Ada Revisi di Bagian TI","is_approved_atasan":false,"last_updated_by":"Zain Maulana Azmi"}}	2021-07-22 09:04:21	2021-07-22 09:04:21
119	default	updated	App\\TransaksiUpload	3	App\\User	2	{"attributes":{"id_user_upload":1,"id_status_rkap":2,"id_user_approver":2,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi_v2.docx","catatan_upload":"Ada Revisi di Bagian TI","is_approved_atasan":true,"last_updated_by":"Zain Maulana Azmi"},"old":{"id_user_upload":1,"id_status_rkap":4,"id_user_approver":2,"tahun_dok":2021,"tipe_file_dok":"docx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi_v2.docx","catatan_upload":"Ada Revisi di Bagian TI","is_approved_atasan":true,"last_updated_by":"Zain Maulana Azmi"}}	2021-07-22 09:04:39	2021-07-22 09:04:39
120	default	created	App\\TransaksiUploadHistory	11	App\\User	2	{"attributes":{"id_uploadrkapql":3,"file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi_v2.docx","versi_file_dok":2}}	2021-07-22 09:04:39	2021-07-22 09:04:39
121	default	updated	App\\TransaksiUpload	4	App\\User	3	{"attributes":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"pptx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.pptx","catatan_upload":"Upload PPT Kedua","is_approved_atasan":true,"last_updated_by":"Zain Maulana Azmi"},"old":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"pptx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.pptx","catatan_upload":"Upload PPT Kedua","is_approved_atasan":false,"last_updated_by":"Zain Maulana Azmi"}}	2021-07-22 09:06:02	2021-07-22 09:06:02
122	default	updated	App\\TransaksiUpload	4	App\\User	2	{"attributes":{"id_user_upload":1,"id_status_rkap":2,"id_user_approver":2,"tahun_dok":2021,"tipe_file_dok":"pptx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.pptx","catatan_upload":"Upload PPT Kedua","is_approved_atasan":true,"last_updated_by":"Zain Maulana Azmi"},"old":{"id_user_upload":1,"id_status_rkap":1,"id_user_approver":null,"tahun_dok":2021,"tipe_file_dok":"pptx","file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.pptx","catatan_upload":"Upload PPT Kedua","is_approved_atasan":true,"last_updated_by":"Zain Maulana Azmi"}}	2021-07-22 09:06:16	2021-07-22 09:06:16
123	default	created	App\\TransaksiUploadHistory	12	App\\User	2	{"attributes":{"id_uploadrkapql":4,"file_dok":"RKAP_2021_Sistem, TI & Riset Teknologi.pptx","versi_file_dok":1}}	2021-07-22 09:06:16	2021-07-22 09:06:16
\.


--
-- TOC entry 2244 (class 0 OID 44500)
-- Dependencies: 187
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.failed_jobs (id, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- TOC entry 2239 (class 0 OID 44472)
-- Dependencies: 182
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_08_19_000000_create_failed_jobs_table	1
4	2021_07_06_040854_create_permission_tables	2
5	2021_07_13_101721_create_activity_log_table	3
\.


--
-- TOC entry 2249 (class 0 OID 44536)
-- Dependencies: 192
-- Data for Name: model_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.model_has_permissions (permission_id, model_type, model_id) FROM stdin;
\.


--
-- TOC entry 2250 (class 0 OID 44547)
-- Dependencies: 193
-- Data for Name: model_has_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.model_has_roles (role_id, model_type, model_id) FROM stdin;
2	App\\User	1
1	App\\User	2
3	App\\User	3
\.


--
-- TOC entry 2242 (class 0 OID 44491)
-- Dependencies: 185
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- TOC entry 2246 (class 0 OID 44512)
-- Dependencies: 189
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.permissions (id, name, guard_name, created_at, updated_at, id_divisi, nama_menu, nama_sub_menu) FROM stdin;
1	01_menu	web	2021-07-08 05:01:39	2021-07-08 05:05:32	1	Master Data	\N
\.


--
-- TOC entry 2251 (class 0 OID 44558)
-- Dependencies: 194
-- Data for Name: role_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role_has_permissions (permission_id, role_id) FROM stdin;
1	1
1	2
1	3
\.


--
-- TOC entry 2248 (class 0 OID 44525)
-- Dependencies: 191
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name, guard_name, created_at, updated_at) FROM stdin;
1	Admin	web	2021-07-08 05:05:47	2021-07-12 04:59:09
2	User	web	2021-07-16 09:52:01	2021-07-16 09:52:01
3	EVP	web	2021-07-22 02:35:20	2021-07-22 02:35:20
\.


--
-- TOC entry 2253 (class 0 OID 44575)
-- Dependencies: 196
-- Data for Name: tblm_divisi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tblm_divisi (id_divisi, nama_divisi, kode_divisi, lantai_divisi, image_divisi, url_divisi, no_urut_divisi) FROM stdin;
1	Sistem, TI & Riset Teknologi	SIT	8	\N	\N	1
2	Corporate Planning	CP	7	\N	\N	2
\.


--
-- TOC entry 2257 (class 0 OID 44598)
-- Dependencies: 200
-- Data for Name: tblm_scheduleclosing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tblm_scheduleclosing (id_scheduleclosing, id_divisi, jenis_rkap, tahun_dok, waktu_closing, created_on, last_updated_on) FROM stdin;
1	\N	Kualitatif	2021	2021-07-23 23:55:00	2021-07-09 11:11:08	2021-07-22 11:16:32
\.


--
-- TOC entry 2255 (class 0 OID 44587)
-- Dependencies: 198
-- Data for Name: tblm_templatedokumen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tblm_templatedokumen (id_template_dok, id_divisi, nama_template_dok, jenis_rkap, tahun_dok, tipe_file_dok, file_dok, created_on, last_updated_on) FROM stdin;
7	1	RKAP_SIT	Kualitatif	2021	pptx	RKAP_SIT_2021.pptx	2021-07-15 04:27:21	2021-07-15 04:27:21
8	1	RKAP_SIT	Kuantitatif	2021	xlsx	RKAP_SIT_2021.xlsx	2021-07-15 04:42:45	2021-07-15 04:42:45
9	1	RKAP_SIT_V2	Kualitatif	2021	docx	RKAP_SIT_2021.docx	2021-07-15 04:42:58	2021-07-16 16:58:37
10	2	RKAP_CP	Kualitatif	2021	docx	RKAP_CP_2021.docx	2021-07-16 09:59:39	2021-07-16 09:59:39
\.


--
-- TOC entry 2263 (class 0 OID 45291)
-- Dependencies: 206
-- Data for Name: tblt_uploadrkapql; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tblt_uploadrkapql (id_uploadrkapql, id_user_upload, id_status_rkap, id_user_approver, tahun_dok, tipe_file_dok, file_dok, catatan_upload, is_approved_atasan, created_on, last_updated_by, last_updated_on) FROM stdin;
3	1	2	2	2021	docx	RKAP_2021_Sistem, TI & Riset Teknologi_v2.docx	Ada Revisi di Bagian TI	t	2021-07-22 08:43:30	Zain Maulana Azmi	2021-07-22 16:04:39
4	1	2	2	2021	pptx	RKAP_2021_Sistem, TI & Riset Teknologi.pptx	Upload PPT Kedua	t	2021-07-22 08:56:12	Zain Maulana Azmi	2021-07-22 16:06:15
\.


--
-- TOC entry 2261 (class 0 OID 44664)
-- Dependencies: 204
-- Data for Name: tblt_uploadrkapql_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tblt_uploadrkapql_history (id_uploadrkapql_history, id_uploadrkapql, file_dok, versi_file_dok, created_on) FROM stdin;
10	3	RKAP_2021_Sistem, TI & Riset Teknologi.docx	1	2021-07-22 08:56:42
11	3	RKAP_2021_Sistem, TI & Riset Teknologi_v2.docx	2	2021-07-22 09:04:39
12	4	RKAP_2021_Sistem, TI & Riset Teknologi.pptx	1	2021-07-22 09:06:16
\.


--
-- TOC entry 2241 (class 0 OID 44480)
-- Dependencies: 184
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, id_role, id_divisi) FROM stdin;
1	Zain Maulana Azmi	zain.maulana@hutamakarya.com	\N	$2y$10$PE3WnS2Gx./ZJCijAYuKE.8nPTuloeuRx9KVuUqYsImgxNoZW0A92	\N	2021-07-07 02:33:35	2021-07-16 09:52:44	2	1
2	Corporate Planning	corplan.kp@hutamakarya.com	\N	$2y$10$tN/H9pwlE3/uoUZgPFupiuvbULIFs.SQxKcuxZKz/4BOzshIxLo8G	\N	2021-07-16 09:52:28	2021-07-16 09:52:48	1	2
3	EVP SIT	divisi.sit@hutamakarya.com	\N	$2y$10$rcANgrZEAt24vgLx0PSO6eTCn2ZwPiw3E.jZ96v1PkY71jOLAjC.O	\N	2021-07-22 02:38:22	2021-07-22 02:38:22	3	1
\.


--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 201
-- Name: activity_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.activity_log_id_seq', 123, true);


--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 186
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- TOC entry 2283 (class 0 OID 0)
-- Dependencies: 181
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 5, true);


--
-- TOC entry 2284 (class 0 OID 0)
-- Dependencies: 188
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.permissions_id_seq', 1, true);


--
-- TOC entry 2285 (class 0 OID 0)
-- Dependencies: 190
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 3, true);


--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 195
-- Name: tblm_divisi_id_divisi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tblm_divisi_id_divisi_seq', 2, true);


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 199
-- Name: tblm_scheduleclosing_id_scheduleclosing_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tblm_scheduleclosing_id_scheduleclosing_seq', 1, true);


--
-- TOC entry 2288 (class 0 OID 0)
-- Dependencies: 197
-- Name: tblm_templatedokumen_id_template_dok_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tblm_templatedokumen_id_template_dok_seq', 10, true);


--
-- TOC entry 2289 (class 0 OID 0)
-- Dependencies: 203
-- Name: tblt_uploadrkapql_history_id_uploadrkapql_history_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tblt_uploadrkapql_history_id_uploadrkapql_history_seq', 12, true);


--
-- TOC entry 2290 (class 0 OID 0)
-- Dependencies: 205
-- Name: tblt_uploadrkapql_id_uploadrkapql_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tblt_uploadrkapql_id_uploadrkapql_seq', 4, true);


--
-- TOC entry 2291 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 3, true);


--
-- TOC entry 2113 (class 2606 OID 44617)
-- Name: activity_log activity_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activity_log
    ADD CONSTRAINT activity_log_pkey PRIMARY KEY (id);


--
-- TOC entry 2088 (class 2606 OID 44509)
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- TOC entry 2081 (class 2606 OID 44477)
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2099 (class 2606 OID 44546)
-- Name: model_has_permissions model_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);


--
-- TOC entry 2102 (class 2606 OID 44557)
-- Name: model_has_roles model_has_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);


--
-- TOC entry 2090 (class 2606 OID 44522)
-- Name: permissions permissions_name_guard_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_name_guard_name_unique UNIQUE (name, guard_name);


--
-- TOC entry 2092 (class 2606 OID 44520)
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2104 (class 2606 OID 44572)
-- Name: role_has_permissions role_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);


--
-- TOC entry 2094 (class 2606 OID 44535)
-- Name: roles roles_name_guard_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_name_guard_name_unique UNIQUE (name, guard_name);


--
-- TOC entry 2096 (class 2606 OID 44533)
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2106 (class 2606 OID 44583)
-- Name: tblm_divisi tblm_divisi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_divisi
    ADD CONSTRAINT tblm_divisi_pkey PRIMARY KEY (id_divisi);


--
-- TOC entry 2110 (class 2606 OID 44606)
-- Name: tblm_scheduleclosing tblm_scheduleclosing_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_scheduleclosing
    ADD CONSTRAINT tblm_scheduleclosing_pkey PRIMARY KEY (id_scheduleclosing);


--
-- TOC entry 2108 (class 2606 OID 44595)
-- Name: tblm_templatedokumen tblm_templatedokumen_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_templatedokumen
    ADD CONSTRAINT tblm_templatedokumen_pkey PRIMARY KEY (id_template_dok);


--
-- TOC entry 2117 (class 2606 OID 44672)
-- Name: tblt_uploadrkapql_history tblt_uploadrkapql_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblt_uploadrkapql_history
    ADD CONSTRAINT tblt_uploadrkapql_history_pkey PRIMARY KEY (id_uploadrkapql_history);


--
-- TOC entry 2119 (class 2606 OID 45299)
-- Name: tblt_uploadrkapql tblt_uploadrkapql_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblt_uploadrkapql
    ADD CONSTRAINT tblt_uploadrkapql_pkey PRIMARY KEY (id_uploadrkapql);


--
-- TOC entry 2083 (class 2606 OID 44490)
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 2085 (class 2606 OID 44488)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2111 (class 1259 OID 44620)
-- Name: activity_log_log_name_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX activity_log_log_name_index ON public.activity_log USING btree (log_name);


--
-- TOC entry 2114 (class 1259 OID 44619)
-- Name: causer; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX causer ON public.activity_log USING btree (causer_type, causer_id);


--
-- TOC entry 2097 (class 1259 OID 44539)
-- Name: model_has_permissions_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_permissions_model_id_model_type_index ON public.model_has_permissions USING btree (model_id, model_type);


--
-- TOC entry 2100 (class 1259 OID 44550)
-- Name: model_has_roles_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_roles_model_id_model_type_index ON public.model_has_roles USING btree (model_id, model_type);


--
-- TOC entry 2086 (class 1259 OID 44497)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- TOC entry 2115 (class 1259 OID 44618)
-- Name: subject; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX subject ON public.activity_log USING btree (subject_type, subject_id);


--
-- TOC entry 2120 (class 2606 OID 44540)
-- Name: model_has_permissions model_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- TOC entry 2121 (class 2606 OID 44551)
-- Name: model_has_roles model_has_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- TOC entry 2122 (class 2606 OID 44561)
-- Name: role_has_permissions role_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- TOC entry 2123 (class 2606 OID 44566)
-- Name: role_has_permissions role_has_permissions_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- TOC entry 2269 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2021-07-22 16:25:44

--
-- PostgreSQL database dump complete
--

