<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'HomeController@index')->name('home');

    Route::group(['middleware' => ['role:User']], function () {
        //Modul Upload
        Route::get('/monitoring_upload', 'UploadController@index')->name('monitoring_upload');
        Route::get('/form_upload', 'UploadController@form_upload')->name('form_upload');
        Route::get('/template_rkap', 'UploadController@template_rkap')->name('template_rkap');
        Route::post('/upload_file', 'UploadController@upload_file');
        Route::post('/upload_revision{id}', 'UploadController@upload_revision');
    });

    Route::group(['middleware' => ['role:EVP']], function () {
        //Modul Upload
        Route::get('/monitoring_submit', 'UploadController@monitoring_submit')->name('monitoring_submit');
        Route::post('/approve_submit{id}', 'UploadController@approve_submit');
    });

    Route::group(['middleware' => ['role:Admin']], function () {
        //Modul Approval
        Route::get('/monitoring_approval', 'ApprovalController@index')->name('monitoring_approval');
        Route::post('/approve_form{id}', 'ApprovalController@approve_form');
        Route::post('/open_approve_form{id}', 'ApprovalController@open_approve_form');

        //MD - User
        Route::get('/masterdata/user', 'MasterDataController@list_user')->name('user');
        Route::post('/masterdata/user/insert','MasterDataController@insert_user');
        Route::post('/masterdata/user/update{id}','MasterDataController@update_user');
        Route::post('/masterdata/user/delete{id}', 'MasterDataController@delete_user');

        //MD - Divisi
        Route::get('/masterdata/divisi', 'MasterDataController@list_divisi')->name('divisi');
        Route::post('/masterdata/divisi/insert','MasterDataController@insert_divisi');
        Route::post('/masterdata/divisi/update{id}','MasterDataController@update_divisi');
        Route::post('/masterdata/divisi/delete{id}', 'MasterDataController@delete_divisi');

        //MD - Permission
        Route::get('/masterdata/permission', 'MasterDataController@list_permission')->name('permission');
        Route::post('/masterdata/permission/insert','MasterDataController@insert_permission');
        Route::post('/masterdata/permission/update{id}','MasterDataController@update_permission');
        Route::post('/masterdata/permission/delete{id}', 'MasterDataController@delete_permission');

        //MD - Role
        Route::get('/masterdata/role', 'MasterDataController@list_role')->name('role');
        Route::post('/masterdata/role/insert','MasterDataController@insert_role');
        Route::post('/masterdata/role/update{id}','MasterDataController@update_role');
        Route::post('/masterdata/role/delete{id}', 'MasterDataController@delete_role');

        //MD - Template Dokumen
        Route::get('/masterdata/templatedok', 'MasterDataController@list_templatedok')->name('templatedok');
        Route::post('/masterdata/templatedok/insert','MasterDataController@insert_templatedok');
        Route::post('/masterdata/templatedok/update{id}','MasterDataController@update_templatedok');
        Route::post('/masterdata/templatedok/delete{id}', 'MasterDataController@delete_templatedok');

        //MD - Schedule Closing Upload
        Route::get('/masterdata/scheduleclosing', 'MasterDataController@list_scheduleclosing')->name('scheduleclosing');
        Route::post('/masterdata/scheduleclosing/insert','MasterDataController@insert_scheduleclosing');
        Route::post('/masterdata/scheduleclosing/update{id}','MasterDataController@update_scheduleclosing');
        Route::post('/masterdata/scheduleclosing/delete{id}', 'MasterDataController@delete_scheduleclosing');
    });
});